//
//  Contact.swift
//  Tapadito
//
//  Created by An Phan on 3/20/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import Foundation
import SwiftyJSON
import Contacts

class Contact {
    let contactId: Int
    let userId: Int
    let contactName: String
    let contactPhone: String
    let country: String
    let createdAt: Date
    let message: Message?
    
    init(name: String, phone: String, country: String) {
        self.contactId = 0
        self.userId = 0
        self.contactName = name
        self.contactPhone = phone
        self.country = country
        self.createdAt = Date()
        self.message = nil
    }
    
    required init(json: JSON) {
        contactId    = json["contactid"].intValue
        userId       = json["userid"].intValue
        contactName  = json["contactname"].stringValue
        contactPhone = json["contactphone"].stringValue
        country      = json["country"].stringValue
        createdAt    = json["createddate"].stringValue.toDateTime()
        message      = Message(json: json["messages_test"])
    }
}

class ContactInPhone {
    let contactId: Int
    let userId: Int
    let contactName: String
    let contactPhone: String
    let country: String
    let createdAt: Date
    let message: Message?
    init(contactId: Int, userId: Int, contactName: String, contactPhone: String, country: String, createdAt: Date, message: Message) {
        self.contactId = contactId
        self.userId = userId
        self.contactName = contactName
        self.contactPhone = contactPhone
        self.country = country
        self.createdAt = createdAt
        self.message = message
    }
}

class ContactFromPhone {
    let name: String
    let number: String
    
    init(name: String, number: String) {
        self.name = name
        self.number = number
    }
}

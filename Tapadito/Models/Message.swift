//
//  Messages.swift
//  Tapadito
//
//  Created by An Phan  on 3/15/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import SwiftyJSON

class Message {
    var id: Int
    var msgFrom: String
    var msgTo: String
    var content: String
    var date: Date
    var userId: Int
    var direction: DirectionMessage
    
    required init(json: JSON) {
        id          = json["messageid"].intValue
        msgFrom     = json["msgfrom"].stringValue
        msgTo       = json["msgto"].stringValue
        content     = json["message"].stringValue
        date        = json["msgdate"].stringValue.toDateTime()
        userId      = json["userid"].intValue
        direction   = DirectionMessage(rawValue: json["direction"].stringValue) ?? DirectionMessage.InBound
    }
}

struct Content {
    var content: String
    var time : Int
    init(content: String, time: Int) {
        self.content = content
        self.time = time
    }
}

struct UserMessages {
    var user: User
    var content: [Content]
    
    init(user: User, content: [Content]) {
        self.user = user
        self.content = content
    }
}

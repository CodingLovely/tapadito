//
//  Country.swift
//  Tapadito
//
//  Created by An Phan  on 3/20/19.
//  Copyright © 2019 Tapadito. All rights reserved.


import Foundation

struct ResponseData: Decodable {
    var person: [Country]
}

struct Country : Decodable {
    var code: String
    var dial_code: String
    var name: String
}

//
//  Home.swift
//  Tapadito
//
//  Created by An Phan  on 3/17/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

struct HomeType {
    var titleImage: String
    var subTitle: String
    var desc : String
    
    init(titleImage: String, subTitle: String, desc: String) {
        self.titleImage = titleImage
        self.subTitle = subTitle
        self.desc = desc
    }
}
struct HeaderDesc {
    var title: String
    var desc: [String]
    init(title: String, desc: [String]) {
        self.title = title
        self.desc = desc
    }
}

struct FooterDesc {
    var title: String
    var desc: [String]
    init(title: String, desc: [String]) {
        self.title = title
        self.desc = desc
    }
}

//
//  Home.swift
//  Tapadito
//
//  Created by An Phan  on 3/17/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

let homeList: [HomeType] = [
    HomeType(titleImage: "price", subTitle:"Sumamente barato"
       , desc: """
            Es super barato usar a Tapadito en los Estados Unidos y en todos los paises de habla español!
            Solo cuesta el enviar un mensaje.
            El recibir mensajes de texto por Tapadito es completamente gratis.
            """),
    HomeType(titleImage: "features" , subTitle: "Sumamente flexible", desc: """
            Tapadito funciona aquí en los Estados Unidos y en todos nuestros países.
            Funciona en •Santo Domingo, •PuertoRico, •Colombia, •Chile, •Peru, •Venezuela, •Mexico, •Argentina, •Ecuador, •Guatemala, •Cuba, •Bolivia, •Honduras, •El Salvador, •Nicaragua, •Costa Rica, •Panama, •Uruguay, •Paraguay, •Jamaica - hasta en España!
            Funciona como textos regulares - pero proteje su número personal verdadero!
            """),
    HomeType(titleImage: "testimonials", subTitle: "Clientes Satisfechos", desc: """
            Que alivio!
            Conocí a un muchacho por el Internet y gracias a Tapadito pude mantenerme en contacto con el sin tener que darle mi número de telefono verdadero hasta que lo llegué a conocer mejor!"
            Maria M.
            """)

]

let footerDesc: FooterDesc =
    FooterDesc(title: "Que es Tapadito?", desc: [
    """
    Con Tapadito, usted puede comunicarse con personas sin darles su número de teléfono verdadero
    Solo necesita:
    1. Crear su cuenta
    2. Reciba su "Tapa Número"
    3. Envie y reciba mensajes de texto usando su "Tapa Número"
    No tiene que dar su número de teléfono verdadero.
    Tapadito funciona en su teléfono celular como si fuera cualquiera otra conversación por medio de mensajes de texto, pero con la protección de no tener que dar su numero personal verdadero.
    """
        ])

let headerDesc: HeaderDesc =
HeaderDesc(title: "Tapadito - Tápame!", desc: ["Ahora con Tapadito usted puede enviar y recibir mensajes de texto ¡sin tener que dar su número de teléfono verdadero!", "¡Su número de teléfono verdadero se que \"Tapadito\"!"])

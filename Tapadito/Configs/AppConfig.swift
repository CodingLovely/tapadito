//
//  AppConfig.swift
//  OneCapture
//
//  Created by An Phan on 2/2/19.
//  Copyright © 2019 Next Idea Tech. All rights reserved.
//

import Foundation

enum BuildTarget {
    case debug, adhoc, test, release
}

struct AppConfig {
    struct ServerHosts {
        static let Staging = "https://tapadito.com/api/"
        static let Production = "https://tapadito.com/api/"
    }
    
    #if DEBUG
    static let buildTarget: BuildTarget = .debug
    struct Environment {
        static let hostPath: String = ServerHosts.Staging
    }
    #elseif ADHOC
    static let buildTarget: BuildTarget = .adhoc
    struct Environment {
        static let hostPath = ServerHosts.Production
    }
    #elseif RELEASE
    static let buildTarget: BuildTarget = .release
    struct Environment {
        static let hostPath = ServerHosts.Production
    }
    #else // Compiler Flag not set!!!
    static let buildTarget: BuildTarget = .debug
    struct Environment {
        static let hostPath = ServerHosts.Staging
    }
    #endif
}

//
//  Constants.swift
//  Tapadito
//
//  Created by An Phan on 3/18/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import Foundation

struct WebApi {
    static let ipDetect = "http://api.ipstack.com/check?access_key=ea182a77c89af2c6ad3c35a821ee28e8"
}

struct WebUrl {
    static let price = "https://tapadito.com/pricing.html"
    static let features = "https://tapadito.com/features.html"
    static let getPhone = "https://tapadito.com/phone_page1.html"
    static let landingVideo = "https://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4"
    static let fag = "https://tapadito.com/faq.html"
}

struct Constants {
    static let productIds: Set<String> = Set(arrayLiteral: "com.tapadito.messages.30", "com.tapadito.messages.60", "com.tapadito.messages.120", "com.tapadito.messages.300")
    static let firstIndexedPage = 1
    static let messagePerPage = 100
    static let paypalClientIdSandbox = "EDh5iv1jD4AoZwi34oqn-iN7SWlLW2JhCo2_grEtMAO2IRTHs1JM6ZktMlnT6nyUNjmZ_Zbytdl06xaz"
    static let paypalClientIdProd = "ARW42jhSQktYeXVo_62VhwXgOwTFTAnh0TYDCWECuFO8zPq46GWQdgSO-X3kFYayaX6239alxmjsaIeC"
}

enum DirectionMessage: String {
    case InBound = "inbound", OutBound = "outbound"
}

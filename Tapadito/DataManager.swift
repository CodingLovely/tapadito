//
//  DataManager.swift
//  Tapadito
//
//  Created by An Phan on 3/27/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import Foundation

class DataManager {
    static let shared  = DataManager()
    
    var currentUser: User!
    
    var firstSignUp = false
}

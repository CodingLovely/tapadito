//
//  MessagesTableViewCell.swift
//  Tapadito
//
//  Created by An Phan  on 3/15/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit
import Contacts
import Alamofire

class MessagesTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var namePhoneLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var deleteLabel: UILabel!
    
    static let identifier = "MessageTableViewCell"

    var deleteAction: (() -> Void)?
    
    // MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(deleteTapped))
        deleteLabel.addGestureRecognizer(tapGesture)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func removeCharactor( number: String ) -> String {
        let chars: [Character] = [")", "(", "-", " "]
        let phoneText = number.stringByRemovingAll(characters: chars)
        return phoneText
    }
    
    func renderData(contact: Contact,
                    indexPath: IndexPath,
                    countryCode: String,
                    tempPhoneModel: [ContactFromPhone]) {
        if let msg = contact.message {
            contentLabel.text = msg.content
            timeLabel.text = msg.date.standardTime()
        }
        
        let matchedContacts = tempPhoneModel.filter({
            var tmpPhone = "\(removeCharactor(number: $0.number))"
            if !tmpPhone.hasPrefix("+") {
                tmpPhone = "+\(countryCode)\(removeCharactor(number: $0.number))"
            }
            
            return tmpPhone == contact.contactPhone })
        
        let contactName = contact.contactName.trimmingCharacters(in: .whitespacesAndNewlines)
        if let firstContact = matchedContacts.first {
            var tmpPhone = "\(removeCharactor(number: firstContact.number))"
            if !tmpPhone.hasPrefix("+") {
                tmpPhone = "+\(countryCode)\(removeCharactor(number: firstContact.number))"
            }
            
            if contactName == contact.contactPhone && tmpPhone == contact.contactPhone {
                namePhoneLabel.text = "Conversación con: \(firstContact.name)"
            }
            else {
                namePhoneLabel.text = "Conversación con: \(contact.contactName)"
            }
        }
        else {
            namePhoneLabel.text = "Conversación con: \(contact.contactName)"
        }
    }
    
    @objc func deleteTapped() {
        deleteAction?()
    }
}

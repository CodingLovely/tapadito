//
//  ContactDetailTableViewCell.swift
//  Tapadito
//
//  Created by An Phan  on 3/16/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

class MessageDetailTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var wrappedView: UIView!
    @IBOutlet weak var inboundMessageView: UIView!
    @IBOutlet weak var messagelabel: UILabel!
    @IBOutlet weak var inboundMessLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        wrappedView.customRoundify(coner: 10)
        inboundMessageView.customRoundify(coner: 10)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func renderMessageDetail(message: Message) {
        messagelabel.text = message.content
        inboundMessLabel.text = message.content
        timeLabel.text = message.date.standardTime()
        
        // Updated UI
        messagelabel.isHidden = message.direction == .InBound
        wrappedView.isHidden = message.direction == .InBound
        inboundMessLabel.isHidden = message.direction != .InBound
        inboundMessageView.isHidden = message.direction != .InBound
        timeLabel.textAlignment = message.direction == .InBound ? .left : .right
    }
}

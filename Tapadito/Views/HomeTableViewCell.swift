//
//  HomeTableViewCell.swift
//  Tapadito
//
//  Created by An Phan  on 3/18/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var keyLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    
}

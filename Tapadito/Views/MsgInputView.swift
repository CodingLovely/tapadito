//
//  MsgInputView.swift
//  Tapadito
//
//  Created by An Phan on 3/20/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

protocol CommentInputViewDelegate {
    func commentInputView(_ inputView: MsgInputView, didSentContent: String)
}

class MsgInputView: UIView {

    @IBOutlet weak var textView: UITextView!
    @IBOutlet weak var textViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var placeholderLabel: UILabel!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var bottomBgView: UIView!
    @IBOutlet weak var separateLineView: UIView!
    @IBOutlet weak var buyCreditButton: UIButton!
    @IBOutlet weak var bottomBGViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var placeholderCenterYConstrant: NSLayoutConstraint!
    
    var delegate: CommentInputViewDelegate?
    
    var textViewDidChangedValue:((String) -> Void)?
    var textViewShouldBeginEditing: (() -> Void)?
    var buyButtonAction: (() -> Void)?
    var sendButtonAction: (() -> Void)?
    var commentString: NSString = ""
    
    // MARK: - View life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        buyCreditButton.layer.cornerRadius = 10
        textView.delegate = self
        
    }

    
    // MARK: - IBActions
    
    @IBAction func sendButtonDidTouched(_ sender: AnyObject) {
        textView.textColor = UIColor(hexString: "6f6f6f")
        if let text = textView.text {
            delegate?.commentInputView(self, didSentContent: text)
        }
        sendButtonAction?()
    }
    
    @IBAction func buyButtonAction(_ sender: UIButton) {
        buyButtonAction?()
    }
    
    // MARK: - Methods
    
    func textViewEmpty() {
        placeholderLabel.isHidden = false
        textView.text = ""
        sendButton.activated(false)
    }
    
    func reloadView() {
        if textView.text.isEmpty {
            placeholderLabel.isHidden = false
            sendButton.activated(false)
        }
        else {
            placeholderLabel.isHidden = true
            sendButton.activated(true)
        }
        
        var height = textView.sizeThatFits(CGSize(width: textView.frame.size.width, height: CGFloat.greatestFiniteMagnitude)).height
        
        if height > 120 {   // Max
            height = 120
        }
        if height < 35 {    // Min
            height = 35
        }
        textViewHeightConstraint.constant = height
        
        var tmpFrame = frame
        tmpFrame.size.height = height + 15
        frame = tmpFrame
        textView.reloadInputViews()
    }
}

// MARK: - UITextViewDelegate

extension MsgInputView: UITextViewDelegate {
    func textViewDidChange(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.textColor = UIColor(hexString: "6f6f6f")
        }
        else {
            sendButton.activated(true)
        }
        
        reloadView()
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        //placeholderCenterYConstrant.constant = -22
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        //placeholderCenterYConstrant.constant = 0
        return true
    }
}

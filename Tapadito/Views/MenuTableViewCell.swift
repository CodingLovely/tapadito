//
//  MenuTableViewCell.swift
//  Tapadito
//
//  Created by An Phan on 3/13/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

class MenuTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var itemMenuNameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        itemMenuNameLabel.font = UIFont.openSansBold(size: 17)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

//
//  CreateContactTableViewCell.swift
//  Tapadito
//
//  Created by An Phan  on 3/26/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

class CreateContactTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

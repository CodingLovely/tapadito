//
//  ContactTableViewCell.swift
//  Tapadito
//
//  Created by An Phan  on 3/16/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit


class ContactTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlet
    @IBOutlet weak var userAvatarImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userPhoneNumber: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        userAvatarImage.tintColor = UIColor.grayMainColor()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

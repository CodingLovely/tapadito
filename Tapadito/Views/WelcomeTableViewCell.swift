//
//  HomeTableViewCell.swift
//  Tapadito
//
//  Created by An Phan  on 3/17/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

class WelcomeTableViewCell: UITableViewCell {
    
    // MARK: - IBOutlet
    @IBOutlet weak var wrappedView: UIView!
    @IBOutlet weak var shadowView: UIView!
    
    @IBOutlet weak var topImages: UIImageView!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var descLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        prepareStyle()
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Prepare
    func prepareStyle() {
        wrappedView.customRoundify(coner: 7)
        shadowView.layer.dropShadow()
        topImages.roundCorners(corners: [.topLeft, .topRight], radius: 7)
    }

}

//
//  UIFont.swift
//  Tapadito
//
//  Created by An Phan  on 3/17/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

extension UIFont {
    
    //OpenSans
    class func openSansBold(size s: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Bold", size: s)!
    }
    
    class func openSansRegular(size s: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans", size: s)!
    }
    
    class func openSansItalic(size s: CGFloat) -> UIFont {
        return UIFont(name: "OpenSans-Italic", size: s)!
    }
    
    //OleoScript
    class func oleoScriptBold(size s: CGFloat) -> UIFont {
        return UIFont(name: "OleoScript-Bold", size: s)!
    }
    
    class func oleoScriptRegular(size s: CGFloat) -> UIFont {
        return UIFont(name: "OleoScript-Regular", size: s)!
    }
}

//
//  UILabel.swift
//  Tapadito
//
//  Created by An Phan  on 3/18/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

func  customizeTextStyle(string: String, color: UIColor, size: Int) -> NSMutableAttributedString {
    return NSMutableAttributedString(string: string, attributes:
        [NSAttributedString.Key.foregroundColor : color, NSAttributedString.Key.font: UIFont.openSansBold(size: CGFloat(size)) ])
}



//
//  UIViewController.swift
//  Tapadito
//
//  Created by An Phan on 3/14/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit
import SVProgressHUD

extension UIViewController {
    func showHUD() {
        SVProgressHUD.show()
        showNetworkIndicator()
    }
    
    func dismissHUD(_ delay: TimeInterval? = nil) {
        if let delay = delay {
            SVProgressHUD.dismiss(withDelay: delay)
        }
        else {
            SVProgressHUD.dismiss()
        }
        hideNetworkIndicator()
    }
    
    // MARK: - networkActivityIndicatorVisible
    func showNetworkIndicator() {
        if !UIApplication.shared.isNetworkActivityIndicatorVisible {
            UIApplication.shared.isNetworkActivityIndicatorVisible = true
        }
    }
    
    func hideNetworkIndicator() {
        if UIApplication.shared.isNetworkActivityIndicatorVisible {
            UIApplication.shared.isNetworkActivityIndicatorVisible = false
        }
    }
    
    func translucentNavigationBar() {
        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
        navigationController?.navigationBar.isTranslucent = true
    }
    
    func showAlertWithActions(_ title: String?, message: String?, actions: [UIAlertAction]?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        if let uActios = actions {
            for action in uActios {
                alert.addAction(action)
            }
        }
        else {
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (alertAction) -> Void in }))
        }
        
        present(alert, animated: true, completion: nil)
    }
    
    func isModal() -> Bool {
        if let navigationController = self.navigationController{
            if navigationController.viewControllers.first != self{
                return false
            }
        }
        
        if self.presentingViewController != nil {
            return true
        }
        
        if self.navigationController?.presentingViewController?.presentedViewController == self.navigationController  {
            return true
        }
        
        if self.tabBarController?.presentingViewController is UITabBarController {
            return true
        }
        
        return false
    }
}

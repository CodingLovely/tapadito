//
//  UITableView.swift
//  Tapadito
//
//  Created by An Phan on 3/20/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

extension UITableView {
    func scrollToBottom(_ animated: Bool = true) {
        let section = self.numberOfSections
        if section > 0 {
            let row = self.numberOfRows(inSection: section - 1)
            if row > 0 {
                self.scrollToRow(at: IndexPath(row: row - 1, section: section - 1),
                                 at: .bottom, animated: animated)
            }
        }
    }
}

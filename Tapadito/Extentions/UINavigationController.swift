//
//  UINavigationController.swift
//  Tapadito
//
//  Created by An Phan on 3/27/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

extension UINavigationController {
    func popViewController(animated: Bool, completion: @escaping () -> ()) {
        popViewController(animated: animated)

        if let coordinator = transitionCoordinator, animated {
            coordinator.animate(alongsideTransition: nil) { _ in
                completion()
            }
        } else {
            completion()
        }
    }
}

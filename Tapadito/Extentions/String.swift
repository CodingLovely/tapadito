//
//  String.swift
//  Tapadito
//
//  Created by An Phan on 3/20/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

extension String {
    
    func stringByRemovingAll(characters: [Character]) -> String {
        return String(self.characters.filter({ !characters.contains($0) }))
    }
    
    func stringByRemovingAll(subStrings: [String]) -> String {
        var resultString = self
        subStrings.map { resultString = resultString.replacingOccurrences(of: $0, with: "") }
        return resultString
    }
    
    func removeTheSpecialCharAtFirst(char: String) -> String {
        if hasPrefix(char){
            return String(dropFirst(char.count))
        }
        return self
    }
    
    func toDateTime() -> Date {
        // Create Date Formatter
        let dateFormatter = DateFormatter()
        
        // Specify Format of String to Parse
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"
        
        // Parse into NSDate
        if let dateFromString = dateFormatter.date(from: self) {
            // Return Parsed Date
            return dateFromString
        }
        else {
            dateFormatter.dateFormat = "yyyy-MM-dd"
            if let dateFromString = dateFormatter.date(from: self) {
                // Return Parsed Date
                return dateFromString
            }
        }
        
        return Date()
    }
    
}

func attributeText(label: UILabel, text: String,
                           font1: UIFont, color1: String, range1: String,
                           font2: UIFont, color2: String, range2s: [String],
                            font3: UIFont, color3: String, range3: String
                            ) -> NSMutableAttributedString {
    //Attribute String
    label.text = text
    let attributedString = NSMutableAttributedString(string: label.text!)
    
    // Attribute 1
    let attributes1: [NSAttributedString.Key : Any] = [NSAttributedString.Key.foregroundColor: UIColor(hexString: color1), NSAttributedString.Key.font: font1]
    let range1 = (label.text! as NSString).range(of: range1)
    attributedString.addAttributes(attributes1, range: range1)
    
    // Attribute 2
    let attributes2: [NSAttributedString.Key : Any] = [NSAttributedString.Key.foregroundColor: UIColor(hexString: color2), NSAttributedString.Key.font: font2]
    for range2 in range2s {
        let range2 = (label.text! as NSString).range(of: range2)
        attributedString.addAttributes(attributes2, range: range2)
            
        }
    
    // Attribute 3
    let attributes3: [NSAttributedString.Key : Any] = [NSAttributedString.Key.foregroundColor: UIColor(hexString: color3), NSAttributedString.Key.font: font3]
    
    var searchRange = NSRange(location: 0, length: text.count)
    var foundRange = NSRange()
    
    while searchRange.location < text.count {
        searchRange.length = text.count - searchRange.location
        foundRange = (text as NSString).range(of: range3, options: NSString.CompareOptions.caseInsensitive, range: searchRange)
        if foundRange.location != NSNotFound {
            // found an occurrence of the substring! do stuff here
            searchRange.location = foundRange.location + foundRange.length
            attributedString.addAttributes(attributes3, range: foundRange)
        }
        else {
            // no more substring to find
            break
        }
    }
    
    return attributedString
}

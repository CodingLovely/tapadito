//
//  UIButton.swift
//  Tapadito
//
//  Created by An Phan on 3/14/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

extension UIButton {
    func activated(_ activated: Bool) {
        isEnabled = activated
        alpha = activated ? 1.0 : 0.5
    }
}

//
//  Notification.Name.swift
//  Tapadito
//
//  Created by An Phan  on 4/9/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import Foundation

extension Notification.Name {
    public struct Notify {
        public static let sessionChanged = Notification.Name(rawValue: "Tapadito.SessionChanged")
        public static let SignUpNewUser  = Notification.Name(rawValue: "Tapadito.SignUpNewUser")
    }
}

//
//  Thread.swift
//  Tapadito
//
//  Created by An Phan on 3/20/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import Foundation

extension Thread {
    static func runInMain(execute: @escaping () -> ()) {
        if !Thread.isMainThread {
            DispatchQueue.main.async {
                execute()
            }
        } else {
            execute()
        }
    }
}

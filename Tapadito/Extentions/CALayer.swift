//
//  UIViewController.swift
//  Tapadito
//
//  Created by An Phan on 3/14/19.
//  Copyright © 2019 An Phan. All rights reserved.
//

import UIKit

extension CALayer {
    func applySketchShadow(color: UIColor = UIColor.black.withAlphaComponent(0.3), alpha: Float = 1,
                           x: CGFloat = -1, y: CGFloat = 1,
                           blur: CGFloat = 6, spread: CGFloat = 0) {
        shadowColor = color.cgColor
        shadowOpacity = alpha
        shadowOffset = CGSize(width: x, height: y)
        shadowRadius = blur / 2.0
        if spread == 0 {
            shadowPath = nil
        } else {
            let dx = -spread
            let rect = bounds.insetBy(dx: dx, dy: dx)
            shadowPath = UIBezierPath(rect: rect).cgPath
        }
        masksToBounds = false
    }
    
    // OUTPUT 1
    func dropShadow(scale: Bool = true) {
        masksToBounds = true
        shadowColor = UIColor.black.cgColor
        shadowOpacity = 0.15
        shadowOffset = CGSize(width: 0, height: 1)
        shadowRadius = 3
        cornerRadius = 7
    }
    
    // OUTPUT 2
    func dropShadow(color: UIColor, opacity: Float = 0.5, offSet: CGSize, radius: CGFloat = 1, scale: Bool = true) {
        masksToBounds = false
        shadowColor = color.cgColor
        shadowOpacity = opacity
        shadowOffset = offSet
        shadowRadius = radius
        
        shadowPath = UIBezierPath(rect: self.bounds).cgPath
        shouldRasterize = true
        rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}

//
//  AppState.swift
//  Tapadito
//
//  Created by An Phan on 3/18/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

struct AppState {
    static func logout() {
        UserDefaults.standard.removeObject(forKey: "com.tapadito.user")
    }
    
    static func clearSessionUser() {
        UserAuth.authToken = nil
        UserAuth.userId = nil
    }
    
    // Authentication
    struct UserAuth {
        static var defaults: UserDefaults! {
            return UserDefaults.standard
        }
        
        static func isAuthenticated() -> Bool {
            if let _ = defaults.object(forKey: "com.tapadito.user") {
                return true
            }
            
            return false
        }
        
        static func sessionUser() -> User? {
            if let userData = defaults.object(forKey: "com.tapadito.user") as? Data {
                do {
                    if let user = try NSKeyedUnarchiver.unarchiveObject(with: userData) as? User {
                        return user
                    }
                    else {
                        return nil
                    }
                }
            }
            
            return nil
        }
        
        static let authTokenKey = "AppState.UserAuth.authToken"
        static let userIdKey    = "AppState.UserAuth.lastUserId"
        
        // Current session user token
        static var authToken: String? {
            get {
                return defaults.object(forKey: authTokenKey) as! String?
            }
            set {
                if (newValue == nil) {
                    defaults.removeObject(forKey: authTokenKey)
                } else {
                    defaults.set(newValue, forKey: authTokenKey)
                }
                defaults.synchronize()
            }
        }
        
        // Current session user id
        static var userId: Int? {
            get {
                return defaults.object(forKey: userIdKey) as! Int?
            }
            set {
                if (newValue == nil) {
                    defaults.removeObject(forKey: userIdKey)
                } else {
                    defaults.set(newValue!, forKey: userIdKey)
                }
                
                defaults.synchronize()
            }
        }
    }
    
    
    static func presentLogin() {
        DispatchQueue.main.async {
            let loginVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignInViewController")
            UIApplication.topViewController()?.present(loginVC, animated: true, completion: nil)
        }
    }
    
    static func presentSignUp() {
        DispatchQueue.main.async {
            let signUpVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SignUpViewController")
            UIApplication.topViewController()?.present(signUpVC, animated: true, completion: nil)
        }
    }
    
    static func setLanding() {
        DispatchQueue.main.async {
            let landingVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "LandingNCID") as! UINavigationController
            UIApplication.topViewController()?.present(landingVC, animated: true, completion: nil)
        }
    }
    
    static func setWelcome() {
        DispatchQueue.main.async {
            let welcomeNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "welcomeNCID") as! UINavigationController
             AppDelegate.shared().sideMenuController.embed(centerViewController: welcomeNC)
        }
    }
    
    static func setHome() {
        DispatchQueue.main.async {
            let homeNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeNCID") as! UINavigationController
            AppDelegate.shared().sideMenuController.embed(centerViewController: homeNC)
        }
    }
}

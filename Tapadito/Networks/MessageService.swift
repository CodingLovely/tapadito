//
//  MessageService.swift
//  Tapadito
//
//  Created by An Phan on 3/20/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import Foundation

class MessageService: BaseService {
    typealias FetchMessageSuccessful = (_ contacts: [Message]) -> Void
    typealias FetchOnlyMessageSuccessful = (_ message: Message) -> Void
    
    static let shared = MessageService()
    
    struct Endpoints {
        static let contacts = "messages"
    }
    
    func fetchMessages(page: Int = 1,
                       limit: Int = 100,
                       onSuccess: @escaping FetchContactSuccessful,
                       onError: @escaping ErrorHandler) {
        _ = ["page": page,
                      "per_page": limit]
        GET(Endpoints.contacts, params: nil).responseSwiftyJSON { (_, _, json, error) in
            if let error = error {
                onError(error)
            }
            else {
                let messages = json.arrayValue.compactMap({ Contact(json: $0) })
                onSuccess(messages)
            }
        }
    }
    
    func sendMessages(country: Country,
                      phoneNumber: String,
                      message: String,
                      onSuccess: @escaping FetchOnlyMessageSuccessful,
                      onError: @escaping ErrorHandler) {
        let params = ["country": country.code,
                      "phoneNumber": phoneNumber,
                      "message": message]
        POST(Endpoints.contacts, params: params).responseSwiftyJSON { (_, _, json, error) in
            if let error = error {
                onError(error)
            }
            else {
                let message = Message(json: json)
                onSuccess(message)
            }
        }
    }
    
    func messageDetail(contact: Contact,
                       onSuccess: @escaping FetchMessageSuccessful,
                       onError: @escaping ErrorHandler) {
        let params = ["phone": contact.contactPhone]
        GET("messages/detail", params: params).responseSwiftyJSON { (_, _, json, error) in
            if let error = error {
                onError(error)
            }
            else {
                let messages = json.arrayValue.compactMap({ Message(json: $0) })
                onSuccess(messages)
            }
        }
    }
    
    func deleteConversation(phone: String,
                            onSuccess: @escaping CompletionHandler,
                            onError: @escaping ErrorHandler) {
        let params = ["phone": phone]
        DELETE("conversation", params: params).responseSwiftyJSON { (_, _, json, error) in
            if let error = error {
                onError(error)
            }
            else {
                onSuccess(json, error)
            }
        }
    }
}

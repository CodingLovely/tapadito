//
//  PackageService.swift
//  Tapadito
//
//  Created by An Phan  on 4/8/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import Foundation

class PackageService: BaseService {
    
    typealias FetchPackageSuccessful = (_ package: [Package]) -> Void
    
    static let shared = PackageService()
    
    struct Endpoints {
        static let package = "packages"
    }
    
    func fetchPackage(country: Any,
                      onSuccess: @escaping FetchPackageSuccessful,
                       onError: @escaping ErrorHandler) {
        let params = ["country": country]
        GET(Endpoints.package, params: params).responseSwiftyJSON { (_, _, json, error) in
            if let error = error {
                onError(error)
            }
            else {
                let packages = json.arrayValue.compactMap({ Package(json: $0) })
                onSuccess(packages)
            }
        }
    }
}



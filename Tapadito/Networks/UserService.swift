//
//  UserService.swift
//  Tapadito
//
//  Created by An Phan on 3/20/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import Foundation

typealias UserLoginSuccessful = (_ user: User) -> Void

class UserService: BaseService {
    static let shared = UserService()
    
    struct Endpoints {
        static let login    = "users/login"
        static let signup   = "users/signup"
    }
    
    func login(email: String,
               password: String,
               onSuccess: @escaping UserLoginSuccessful,
               onError: @escaping ErrorHandler) {
        let params = ["email": email, "password": password]
        POST(Endpoints.login, params: params).responseSwiftyJSON { (_, _, json, error) in
            if let error = error {
                onError(error)
            }
            else {
                let user = User(json: json)
                AppState.UserAuth.userId = user.userId
                AppState.UserAuth.authToken = user.token
                
                // Save user
                DataManager.shared.currentUser = user
                
                // Store user for next login.
                let encoded = NSKeyedArchiver.archivedData(withRootObject: user)
                UserDefaults.standard.set(encoded, forKey: "com.tapadito.user")
                UserDefaults.standard.synchronize()
                
                NotificationCenter.default.post(name: Notification.Name.Notify.sessionChanged, object: nil)
                
                onSuccess(user)
            }
        }
    }
    
    func signup(email: String,
                password: String,
                onSuccess: @escaping UserLoginSuccessful,
                onError: @escaping ErrorHandler) {
        let params = ["email": email, "password": password]
        POST(Endpoints.signup, params: params).responseSwiftyJSON { (_, _, json, error) in
            if let error = error {
                onError(error)
            }
            else {
                let user = User(json: json)
                AppState.UserAuth.userId = user.userId
                AppState.UserAuth.authToken = user.token
                
                DataManager.shared.currentUser = user
                
                // Store user for next login.
                let encoded = NSKeyedArchiver.archivedData(withRootObject: user)
                UserDefaults.standard.set(encoded, forKey: "com.tapadito.user")
                UserDefaults.standard.synchronize()
                
                NotificationCenter.default.post(name: Notification.Name.Notify.sessionChanged, object: nil)
    
                onSuccess(user)
            }
        }
    }
    
    func fetchUserInfo(onSuccess: @escaping UserLoginSuccessful,
                       onError: @escaping ErrorHandler) {
        guard let userId = AppState.UserAuth.userId else { return }
        GET("users/\(userId)", params: nil).responseSwiftyJSON { (_, _, json, error) in
            if let error = error {
                onError(error)
            }
            else {
                let user = User(json: json)
                DataManager.shared.currentUser = user
                
                onSuccess(user)
            }
        }
    }
    
    func forgotPassword(email: String,
                        completionHandler: @escaping CompletionHandler) {
        let params = ["email": email]
        PUT("users/forgot_password", params: params).responseSwiftyJSON { (_, _, json, error) in
            if let error = error {
                completionHandler(nil, error)
            }
            else {
                completionHandler(json, nil)
            }
        }
    }
    
    func newPassword(newPassword: String,
                     completionHandler: @escaping CompletionHandler) {
        let params = ["password": newPassword]
        POST("users/new_password", params: params).responseSwiftyJSON { (_, _, json, error) in
            completionHandler(json, error)
        }
    }
}

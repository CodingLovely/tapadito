//
//  ContactService.swift
//  Tapadito
//
//  Created by An Phan on 3/20/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import Foundation

typealias FetchContactSuccessful = (_ contacts: [Contact]) -> Void
typealias ContactUpdateSuccessful = (_ contacts: Contact) -> Void

class ContactService: BaseService {
    static let shared = ContactService()
    
    struct Endpoints {
        static let contacts = "contacts"
    }
    
    func getContacts(page: Int,
                     limit: Int,
                     onSuccess: @escaping FetchContactSuccessful,
                     onError: @escaping ErrorHandler) {
        _ = ["page": page,
                      "per_page": limit]
        GET(Endpoints.contacts, params: nil).responseSwiftyJSON { (_, _, json, error) in
            if let error = error {
                onError(error)
            }
            else {
                let contacts = json.arrayValue.compactMap({ Contact(json: $0) })
                onSuccess(contacts)
            }
        }
    }
    
    func updateContact(contact: Contact,
                       contactname: String,
                       contactphone: String,
                       country: Country,
                       onSuccess: @escaping ContactUpdateSuccessful,
                       onError: @escaping ErrorHandler) {
        let endpoint = Endpoints.contacts + "/\(contact.contactId)"
        let params = ["contactname": contactname,
                      "contactphone": contactphone,
                      "country": country.code]
        PUT(endpoint, params: params).responseSwiftyJSON { (_, _, json, error) in
            if let error = error {
                onError(error)
            }
            else {
                let contact = Contact(json: json)
                onSuccess(contact)
            }
        }
    }
    
    func deleteContact(contact: Contact,
                       completionHandler: @escaping CompletionHandler) {
        let endpoint = Endpoints.contacts + "/\(contact.contactId)"
        DELETE(endpoint, params: nil).responseSwiftyJSON { (_, _, json, error) in
            completionHandler(json, error)
        }
    }
    
    func addContact(contact: Contact,
                    onSuccess: @escaping ContactUpdateSuccessful,
                    onError: @escaping ErrorHandler) {
        let params = ["contactname": contact.contactName,
                      "contactphone": contact.contactPhone,
                      "country": contact.country]
        POST(Endpoints.contacts, params: params).responseSwiftyJSON { (_, _, json, error) in
            if let error = error {
                onError(error)
            }
            else {
                let contact = Contact(json: json)
                onSuccess(contact)
            }
        }
    }
}

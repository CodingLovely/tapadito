//
//  PaymentService.swift
//  Tapadito
//
//  Created by An Phan on 3/24/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import Foundation

class PaymentService: BaseService {
    typealias CreatePaymentSuccessful = (_ payment: Payment) -> Void
    
    static let shared = PaymentService()
    
    struct Endpoints {
        static let payment = "payment"
    }
    
    func createPayment(credits: Int,
                       amount: Int,
                       onSuccess: @escaping CreatePaymentSuccessful,
                       onError: @escaping ErrorHandler) {
        let params = ["credits": credits,
                      "amount": amount]
        POST(Endpoints.payment, params: params).responseSwiftyJSON { (_, _, json, error) in
            if let error = error {
                onError(error)
            }
            else {
                let payment = Payment(json: json)
                onSuccess(payment)
            }
        }
    }
    
    func capturePayment(payment: Payment,
                        onSuccess: @escaping UserLoginSuccessful,
                        onError: @escaping ErrorHandler) {
        let endpoint = Endpoints.payment + "/\(payment.paymentid)"
        let params = ["invoiceid": payment.invoiceid]
        POST(endpoint, params: params).responseSwiftyJSON { (_, _, json, error) in
            if let error = error {
                onError(error)
            }
            else {
                let user = User(json: json)
                onSuccess(user)
                
                DataManager.shared.currentUser = user
                
                NotificationCenter.default.post(name: Notification.Name.Notify.sessionChanged, object: nil)
            }
        }
    }
}

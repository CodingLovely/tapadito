//
//  DataRequest.swift
//  Tapadito
//
//  Created by An Phan on 3/20/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import Alamofire
import SwiftyJSON

extension DataRequest {
    fileprivate func responseSwiftyJSON(_ completionHandler: @escaping (URLRequest, HTTPURLResponse?, SwiftyJSON.JSON, NSError?) -> Void) {
        responseSwiftyJSON(queue: nil, options:JSONSerialization.ReadingOptions.allowFragments, completionHandler:completionHandler)
    }
    
    public func responseSwiftyJSON(
        queue: DispatchQueue? = nil,
        options: JSONSerialization.ReadingOptions = .allowFragments,
        completionHandler:@escaping (URLRequest, HTTPURLResponse?, SwiftyJSON.JSON, NSError?) -> Void) {
        
        response(queue: queue, responseSerializer: DataRequest.jsonResponseSerializer(options: options), completionHandler: { (response) in
            
            DispatchQueue.global(qos: .default).async(execute: {
                
                var responseJSON: JSON
                var error: NSError?
                if response.result.isFailure {
                    responseJSON = JSON.null
                    if let data = response.data {
                        var errorMsg: String?
                        if let errors = JSON(data)["error"].array {
                            errorMsg = errors.first?.stringValue
                        }
                        else if let errorText = JSON(data)["error"].string {
                            errorMsg = errorText
                        }
                        let msg = errorMsg ?? "As we continue to improve our app, this feature is currently unavailable, please try again later."
                        let userInfo: [String: Any] = [
                            NSLocalizedDescriptionKey :  NSLocalizedString(msg, value: msg, comment: "")
                        ]
                        error = NSError(domain: "com.tapadito.tapadito", code: 401, userInfo: userInfo)
                    }
                }
                else {
                    responseJSON = SwiftyJSON.JSON(response.result.value!)
                }
                (queue ?? DispatchQueue.main).async(execute: {
                    completionHandler(response.request!, response.response, responseJSON, error)
                })
            })
        })
    }
}

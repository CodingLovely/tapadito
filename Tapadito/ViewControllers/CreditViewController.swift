//
//  CreditViewController.swift
//  Tapadito
//
//  Created by An Phan on 3/21/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyStoreKit
import StoreKit

class CreditViewController: UIViewController {
    
    // MARK: - IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var buyNowButton: UIButton!
    @IBOutlet weak var priceContainerView: UIView!
    @IBOutlet weak var choosePriceButton: UIButton!
    @IBOutlet weak var choosePriceContainerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var logoutButton: UIButton!
    
    // MARK: - Private var/let
    private var heightRow: CGFloat = 41
    private var widthCountryViewList: CGFloat = 0
    private var heightCountryViewList: CGFloat = 0
    private var spacing: CGFloat = 15
    
    private var products = [SKProduct]()
    private var selectedProduct: SKProduct!
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Compre aquí más créditos"
        fetchProducts()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        prepareUIView()
        
        tableViewHeightConstraint.constant = (heightCountryViewList * 9.5) / 10
        tableViewWidthConstraint.constant = (widthCountryViewList * 9.5) / 10
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // MARK: - Private methods
    private func fetchProducts() {
        showHUD()
        SwiftyStoreKit.retrieveProductsInfo(Constants.productIds) { (result) in
            self.dismissHUD()
            self.products = Array(result.retrievedProducts.sorted(by: { $0.price.doubleValue < $1.price.doubleValue }))
            if self.products.count > 0 {
                self.selectedProduct = self.products.first
                self.priceLabel.text = "\(self.selectedProduct.localizedTitle) \(self.selectedProduct.localizedPrice!)"
            }
            else {
                self.choosePriceButton.isEnabled = false
                self.buyNowButton.isEnabled = false
                self.buyNowButton.layer.opacity = 0.5
                self.priceLabel.text = "No data"
                self.priceLabel.attributedText = NSAttributedString(string: self.priceLabel.text!, attributes: [NSAttributedString.Key.foregroundColor : UIColor.red])
            }
            self.tableView.reloadData()
        }
    }

    fileprivate func prepareUIView() {
        buyNowButton.layer.cornerRadius = 5
        logoutButton.layer.cornerRadius = 5
        priceContainerView.bordered(withColor: "#c3c3c3", width: 0.3, radius: 5)
        tableView.bordered(withColor: "#c3c3c3", width: 0, radius: 5)
        choosePriceContainerView.layer.applySketchShadow()
        
        widthCountryViewList = view.frame.width - spacing * 2
        heightCountryViewList = heightRow * 4 - 1
        
        logoutButton.isHidden = !isModal()
    }

    fileprivate func getCreditsById(productId: String) -> Int? {
        let components = productId.components(separatedBy: ".")
        if let lastComp = components.last, !lastComp.isEmpty {
            return Int(lastComp)
        }
        
        return nil
    }
    
    // MARK: - IBActions
    @IBAction func logoutButtonAction(_ sender: Any) {
        AppState.logout()
        AppState.clearSessionUser()
        NotificationCenter.default.post(name: Notification.Name.Notify.sessionChanged,
                                        object: nil)
        dismiss(animated: true) {
            AppState.setWelcome()
        }
    }
    
    @IBAction func chooseNumberMessageButtonAction(_ button: UIButton) {
        let width = button.isSelected ? (widthCountryViewList * 9.5) / 10 : widthCountryViewList
        let height = button.isSelected ? (heightCountryViewList * 9.5) / 10 : heightCountryViewList
        choosePriceContainerView.alpha = button.isSelected ? 0 : 1
        choosePriceContainerView.isHidden = button.isSelected
        tableViewWidthConstraint.constant = width
        tableViewHeightConstraint.constant = height
        UIView.animate(withDuration: 0.5) {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
        
        button.isSelected = !button.isSelected
    }
    
    @IBAction func buyNowButtonAction(_ sender: UIButton) {
        guard let product = selectedProduct,
            let credit = getCreditsById(productId: product.productIdentifier) else { return }
        showHUD()
        PaymentService.shared.createPayment(credits: credit,
                                            amount: Int(truncating: product.price),
                                            onSuccess: { (payment) in
            // Handle buy product
            SwiftyStoreKit.purchaseProduct(product.productIdentifier, completion: { (result) in
                switch result {
                case .success(let purchase):
                    print("Purchase Success: \(purchase.productId)")
                    
                    PaymentService.shared.capturePayment(payment: payment,
                                                         onSuccess: { (user) in
                        self.dismissHUD()
                        NotificationBannerView.appearOn(controller: self,
                                                        text: "Tu cuenta está actualizada!",
                                                        animateTime: .middle)
                        delay(1, closure: {
                            if self.isModal() {
                                self.dismiss(animated: true, completion: nil)
                            }
                            else {
                                self.navigationController?.popViewController(animated: true)
                            }
                        })
                    }) { (error) in
                        self.dismissHUD()
                    }
                case .error(let error):
                    self.dismissHUD()
                    NotificationBannerView.appearOn(controller: self,
                                                    text: error.localizedDescription,
                                                    animateTime: .middle, isNavi: true)
                }
            })
        }) { (error) in
            self.dismissHUD()
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        choosePriceButton.isSelected = false
        choosePriceContainerView.isHidden = true
        choosePriceContainerView.alpha = 0
        
        UIView.animate(withDuration: 0.5) {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
    }
}

// MARK: - UITableViewDataSource
extension CreditViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SendMessageTableViewCell", for: indexPath) as! SendMessageTableViewCell
        let product = products[indexPath.row]
        
        cell.nameLabel.text = "\(product.localizedTitle) \(String(describing: product.localizedPrice!))"
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension CreditViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightRow
    }
    
    func tableView(_ tableView: UITableView,
                   didSelectRowAt indexPath: IndexPath) {
        choosePriceButton.isSelected = false
        choosePriceContainerView.isHidden = true
        choosePriceContainerView.alpha = 0
        
        selectedProduct = products[indexPath.row]
        priceLabel.text = "\(selectedProduct.localizedTitle) \(selectedProduct.localizedPrice!)"
    }
}

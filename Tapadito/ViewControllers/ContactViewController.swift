//
//  ContactViewController.swift
//  Tapadito
//
//  Created by An Phan on 3/13/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit
import Contacts
import Alamofire


class ContactViewController: UIViewController {
    
    // MARK: - Variables
    var contacts = [Contact]()
    var filteredContacts = [Contact]()
    
    var user: User?
    var countries = [Country]()
    var jsonCountries = [Country]()
    
    let contactStore = CNContactStore()
    var contactsPhone = [CNContact]()
    var itemRowAction: [UITableViewRowAction] = []
    var tempPhoneModel = [ContactFromPhone]()
    var filteredPhoneModel = [ContactFromPhone]()
    
    var tempNumber: String?
    
   // MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Mis Contactos"
        searchBar.clipsToBounds = true
        searchBar.customRoundify(coner: 20)
        
        loadJson(fileName: "countries")
        fetchContacts()
        prepareSearchBar()
        searchBar.delegate = self
        
        getContactPhone()
        listContactFromPhone()
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        //Used segue and push to Message Detail VC
        if segue.identifier == "UserMessageDetailSegue" {
            _ = segue.destination as! MessageDetailViewController
//            destVC.userMessage = sender as? UserMessages
        }
        //Used segue and push to Contact Detail VC
        else if segue.identifier == "UserDetailSegue" {
            let destVC = segue.destination as! ContactDetailViewController
            destVC.contact = sender as? Contact
            
            // Handle `updated contact` button
            destVC.updatedButtonAction = {
                self.fetchContacts()
            }
        }
        // Show create new contact
        else if segue.identifier == "showCreateContactPage" {
            let createContactVC = segue.destination as! CreateContactViewController
            
            // Handle `save contact` button
            createContactVC.saveContactAction = {
                self.fetchContacts()
            }
        }
        else if segue.identifier == "SendMessageSegue" {
            let messageVC = segue.destination as! SendMessageViewController
            messageVC.phone = sender as? String
        }
    }
    
    // MARK: - Methods
    fileprivate func prepareSearchBar() {
        if let textField = searchBar.subviews.first?.subviews.compactMap({ $0 as? UITextField }).first {
            textField.subviews.first?.isHidden = true
            textField.textColor = UIColor(hexString: "808080")
            textField.layer.backgroundColor = UIColor(hexString: "F3F3F3").cgColor
            textField.layer.cornerRadius = 18
            textField.sizeToFit()
            textField.layer.masksToBounds = true
        }
    }
    
    private func loadJson(fileName: String) {
        if let filePath = Bundle.main.path(forResource: fileName, ofType: "json"), let data = NSData(contentsOfFile: filePath) {
            do {
                let decoder = JSONDecoder()
                self.jsonCountries = try decoder.decode([Country].self, from: data as Data)

            } catch _ {
                // Doing something
            }
        }
    }
    
    func getContactPhone(){
        let keys = [CNContactGivenNameKey,
                    CNContactFamilyNameKey,
                    CNContactPhoneNumbersKey]
        let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])

        do {
            try contactStore.enumerateContacts(with: request) { (contact, stop) in
                self.contactsPhone.append(contact)
            }
        } catch {
            print(error.localizedDescription)
        }
    }
    
    func listContactFromPhone() {
        for i in 0..<contactsPhone.count{
            for phoneNumber in contactsPhone[i].phoneNumbers {
                self.tempNumber = phoneNumber.value.stringValue
            }
            tempPhoneModel.append(ContactFromPhone(name: "\(contactsPhone[i].givenName) \(contactsPhone[i].familyName)", number: tempNumber!))
        }
        
        filteredPhoneModel = tempPhoneModel
    }
    
    func fetchContacts() {
        showHUD()
        ContactService.shared.getContacts(page: 1, limit: 100, onSuccess: { (contacts) in
            self.dismissHUD()
            self.contacts = contacts
            self.filteredContacts = contacts
            self.tableView.reloadData()
        }) { (error) in
            self.dismissHUD()
        }
    }
    
    func showAlert(_ indexPath: IndexPath) {
        let alertController = UIAlertController(title: "Borrar contacto", message: "Estás seguro de que quieres eliminar este contacto?", preferredStyle: .alert)
        
        // Create the actions
        let okAction = UIAlertAction(title: " Sí", style: .default) { UIAlertAction in
            let contact = self.filteredContacts[indexPath.row]
            self.showHUD()
            ContactService.shared.deleteContact(contact: contact, completionHandler: { (json, error) in
                if let error = error {
                    self.dismissHUD()
                    debugPrint(error)
                }
                else {
                    self.dismissHUD()
                    self.filteredContacts.remove(at: indexPath.row)
                    CATransaction.begin()
                    CATransaction.setCompletionBlock({
                        self.tableView.reloadData()
                    })
                    
                    self.tableView.beginUpdates()
                    self.tableView.deleteRows(at: [indexPath], with: .top)
                    self.tableView.endUpdates()
                    CATransaction.commit()
                }
            })
        }
        
        let cancelAction = UIAlertAction(title: "No", style: .cancel) { UIAlertAction in
            // Doing something
        }
        
        // Add the actions
        alertController.addAction(okAction)
        alertController.addAction(cancelAction)
        
        // Present the controller
        self.present(alertController, animated: true, completion: nil)
    }
    
    func formatPhone(phone: String) -> String {
        if phone.hasPrefix("+") {
            for countryCode in jsonCountries {
                if phone.hasPrefix(countryCode.dial_code) {
                    var tmpPhone = phone.replacingOccurrences(of: countryCode.dial_code, with: "")
                    for i in  0..<tmpPhone.count {
                        if i == 3 || i == 7 {
                            tmpPhone.insert("-", at: tmpPhone.index(tmpPhone.startIndex, offsetBy: i))
                        }
                    }
                    return "\(countryCode.dial_code)-\(tmpPhone)"
                }
            }
        }
        else if phone.hasPrefix("0") {
            var tmpPhone = phone.replacingOccurrences(of: "0", with: "")
            for i in  0..<tmpPhone.count {
                if i == 3 || i == 7 {
                    tmpPhone.insert("-", at: tmpPhone.index(tmpPhone.startIndex, offsetBy: i))
                }
            }
            return tmpPhone
        }
        else {
            var tmpPhone = phone
            for i in  0..<tmpPhone.count {
                if i == 3 || i == 7 {
                    tmpPhone.insert("-", at: tmpPhone.index(tmpPhone.startIndex, offsetBy: i))
                }
            }
            return tmpPhone
        }
        
        return phone
    }
    
    func removeCharactor( number: String ) -> String {
        let chars: [Character] = [")", "(", "-", " "]
        let phoneText = number.stringByRemovingAll(characters: chars)
        return phoneText
    }
}

// MARK: - UITableViewDataSource
extension ContactViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return contactsPhone.count > 0 ? 2 : 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return filteredContacts.count
        case 1:
            return filteredPhoneModel.count
        default:
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactTableViewCell", for: indexPath) as! ContactTableViewCell
        
        if indexPath.section == 0 {
            let contact = filteredContacts[indexPath.row]
            let formattedPhone = formatPhone(phone: contact.contactPhone)
            
            if contact.contactName.count > 0
                && contact.contactName != contact.contactPhone{
                cell.userNameLabel.text =  contact.contactName
            } else if contact.contactName == contact.contactPhone{
                cell.userNameLabel.text =  formattedPhone
            } else {
                cell.userNameLabel.text =  "No Name"
            }
            
            cell.userPhoneNumber.text = formattedPhone
        }
        else if indexPath.section == 1 {
            let phone = filteredPhoneModel[indexPath.row].number
            let tmpPhone = removeCharactor(number: phone)
            let formattedPhone = formatPhone(phone: tmpPhone)
            
            let teamPhone = filteredPhoneModel[indexPath.row]
            let name = teamPhone.name.trimmingCharacters(in: .whitespacesAndNewlines)
            
            if !name.isEmpty &&  name != phone {
                cell.userNameLabel.text = name
            }
            else if filteredPhoneModel[indexPath.row].name == phone {
                cell.userNameLabel.text = formattedPhone
            }
            else {
                cell.userNameLabel.text = "No Name"
            }
            cell.userPhoneNumber.text = formattedPhone
        }
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension ContactViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let view = UIView(frame: CGRect(x:0, y:0, width:tableView.frame.size.width, height:50))
        let label = UILabel(frame: CGRect(x:20, y:12, width:tableView.frame.size.width, height:30))
        label.font = UIFont.openSansRegular(size: 20)
        
        if section == 0{
            label.text = "Contactos tapadito"
        } else {
            label.text = "Otros contactos"
        }
        
        view.addSubview(label)
        view.backgroundColor = UIColor(hexString: "D8D8D8")
        
        return view
        
    }
    
     func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        
        if indexPath.section == 0 {
            //Initial Detelet Action
            let deleteAction = UITableViewRowAction(style: .destructive, title: "Borrar") { (action, indexPath) in
                self.showAlert(indexPath)
            }
            
            //Initial Message Action
            let messageAction = UITableViewRowAction(style: .default, title: "Mensajes") { (action, indexPath) in
                let messageDetail = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MessageDetailVC") as! MessageDetailViewController
                messageDetail.contact = self.filteredContacts[indexPath.row]
                
                self.navigationController?.pushViewController(messageDetail, animated: true)
            }
            
            messageAction.backgroundColor = UIColor.brown
            itemRowAction = [deleteAction, messageAction]
        } else {
            itemRowAction = []
        }
        
        return itemRowAction
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let contact = filteredContacts[indexPath.row]
            performSegue(withIdentifier: "UserDetailSegue", sender: contact)
        }
        else if indexPath.section == 1 {
            let contact = filteredPhoneModel[indexPath.row]
            performSegue(withIdentifier: "SendMessageSegue", sender: contact.number)
        }
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}

// MARK: - UISearchBarDelegate
extension ContactViewController: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if let searchText = searchBar.text {
            if searchText.count == 0 {
                filteredContacts = contacts
                filteredPhoneModel = tempPhoneModel
            }
            else {
                filteredContacts = contacts.filter {
                    $0.contactPhone.lowercased().contains(searchText.lowercased())
                    || $0.contactName.lowercased().contains(searchText.lowercased())
                }
                
                filteredPhoneModel = tempPhoneModel.filter {
                    $0.name.lowercased().contains(searchText.lowercased())
                    || $0.number.lowercased().contains(searchText.lowercased())
                }
            }
        }
        
        tableView.reloadData()
    }
}

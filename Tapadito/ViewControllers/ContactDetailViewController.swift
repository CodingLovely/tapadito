//
//  ContactDetailViewController.swift
//  Tapadito
//
//  Created by An Phan  on 3/17/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

class ContactDetailViewController: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var contactNameTextField: UITextField!
    @IBOutlet weak var contactPhoneTextField: UITextField!
    @IBOutlet weak var saveContactButton: UIButton!
    @IBOutlet weak var wrappedTableView: UIView!
    @IBOutlet weak var countrySelectButton: UIButton!
    @IBOutlet weak var wrappedSelectView: UIView!
    @IBOutlet weak var countryTableView: UITableView!
    @IBOutlet weak var nameSelectLabel: UILabel!
    @IBOutlet weak var codeSelectLabel: UILabel!
    @IBOutlet weak var tableViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Variable
    var contact: Contact!
    var countries = [Country]()
    var country: Country!
    var nameFirstShow: String!
    var tempNum: String!
    var tempCode: String!
    var tempDialCode: String!
    var newDialCode: String!
    var updatedButtonAction: (() -> Void)?
    
    // MARK: - Private var/let
    private var heightRow: CGFloat = 41
    private var widthCountryViewList: CGFloat = 0
    private var heightCountryViewList: CGFloat = 0
    private var spacing: CGFloat = 15
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Detalles del contacto"
        
        settupUI()
        loadJson(fileName: "countries")
        prepareStyle()
        defaultFrame()
        renderData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        nameFirstShow = nameSelectLabel.text!
        tableViewHeightConstraint.constant = (heightCountryViewList * 9.5) / 10
        tableViewWidthConstraint.constant = (widthCountryViewList * 9.5) / 10
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        tapRootView()
        wrappedTableView.alpha = 0
        wrappedTableView.isHidden = true
        countrySelectButton.isSelected = false
    }
    
    // MARK: - IBAction
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        
        UIView.setAnimationsEnabled(false)
        
        if let name = contactNameTextField.text,
            let phone = contactPhoneTextField.text {
            DispatchQueue.main.async {
                self.saveContactButton.activated(name != self.contact.contactName  || phone != self.contact.contactPhone)
            }
        }
    }
    
    @IBAction func selectCountryAction(_ button: UIButton) {
        tapRootView()
        
        let width = button.isSelected ? (widthCountryViewList * 9.5) / 10 : widthCountryViewList
        let height = button.isSelected ? (heightCountryViewList * 9.5) / 10 : heightCountryViewList
        wrappedTableView.alpha = button.isSelected ? 0 : 1
        wrappedTableView.isHidden = button.isSelected
        tableViewWidthConstraint.constant = width
        tableViewHeightConstraint.constant = height
        
        UIView.animate(withDuration: 0.5) {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
        
        button.isSelected = !button.isSelected
    }
    
    
    @IBAction func saveContactAction(_ sender: UIButton) {
        showHUD()
        if let name = contactNameTextField.text,
            let phone = contactPhoneTextField.text {
            ContactService.shared.updateContact(contact: contact,
                                                contactname: name,
                                                contactphone: phone,
                                                country: country,
                                                onSuccess: { (contact) in
                self.dismissHUD()
                self.view.endEditing(true)
                self.navigationController?.popViewController(animated: true,
                                                             completion: {
                    self.updatedButtonAction?()
                })
            }) { (error) in
                self.dismissHUD()
                DispatchQueue.main.async {
                    NotificationBannerView.appearOn(controller: self, text: error.localizedDescription)
                }
            }
        }
    }
    
    // MARK: - Prepare
    func prepareStyle(){
        saveContactButton.customRoundify(coner: 7)
        
        countryTableView.layer.cornerRadius = 5
        wrappedSelectView.bordered(withColor: "#c3c3c3", width: 0.3, radius: 5)
        
        wrappedTableView.layer.applySketchShadow()
        
    }
    
    // MARK: - Private Methods
    private func tapRootView() {
        view.endEditing(true)
    }
    
    private func loadJson(fileName: String) {
        if let filePath = Bundle.main.path(forResource: fileName, ofType: "json"), let data = NSData(contentsOfFile: filePath) {
            do {
                let decoder = JSONDecoder()
                self.countries = try decoder.decode([Country].self, from: data as Data)
            } catch _ {
                
            }
        }
    }
    
    private func settupUI(){
        contactNameTextField.delegate = self
        contactPhoneTextField.delegate = self
        
        contactNameTextField.resignFirstResponder()
        contactPhoneTextField.resignFirstResponder()
        
        for (_ , item) in countries.enumerated() {
            if contact.country == item.code{
                nameSelectLabel.text = item.name
                codeSelectLabel.text = item.code
            }
        }
        
        saveContactButton.activated(false)
        wrappedTableView.isHidden = true
        countrySelectButton.isSelected = false
        wrappedTableView.alpha = 0
    }
    
    private func defaultFrame() {
        // frame relationship list
        widthCountryViewList = view.frame.width - spacing * 2
        heightCountryViewList = heightRow * 4 - 1
    }
    
    private func renderData() {
        contactNameTextField.text = contact.contactName
        contactPhoneTextField.text = contact.contactPhone

        // Get country from countries
        if let countryIndex = countries.firstIndex(where: { $0.code == contact.country }) {
            country = countries[countryIndex]
            codeSelectLabel.text = country.code
            nameSelectLabel.text = country.name
        }
    }
}

// MARK: - UITextFieldDelegate
extension ContactDetailViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == contactNameTextField {
            contactPhoneTextField.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if countrySelectButton.isSelected == true && wrappedTableView.isHidden == false {
            countrySelectButton.isSelected = !countrySelectButton.isSelected
            wrappedTableView.isHidden = !countrySelectButton.isSelected
        }
        
        UIView.animate(withDuration: 0.5) {
            self.wrappedTableView.layoutIfNeeded()
        }
        
        return true
    }
}

// MARK: - UITableViewDataSource
extension ContactDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ContactDetailTableViewCell", for: indexPath) as! ContactDetailTableViewCell
        cell.nameLabel.text = countries[indexPath.row].name
        cell.codeLabel.text = countries[indexPath.row].code
        return cell
    }
}

// MARK: - UITableViewDelegate
extension ContactDetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightRow
    }
    
    func tableView(_ tableView: UITableView, willSelectRowAt indexPath: IndexPath) -> IndexPath? {
        
         tempNum = contactPhoneTextField.text
         tempCode = codeSelectLabel.text
        
        //Get Current Dial Code
        for (_, item) in countries.enumerated() {
            if tempCode == item.code{
                tempDialCode = item.dial_code
            }
        }
        return indexPath
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        wrappedTableView.isHidden = true
        countrySelectButton.isSelected = false
        
        // Update data of country
        nameSelectLabel.text = countries[indexPath.row].name
        codeSelectLabel.text = countries[indexPath.row].code
        country = countries[indexPath.row]
        
        // Display new contact phone number
        if let countryIndex = countries.firstIndex(where: { $0.code == contact.country }) {
            let currentCountry = countries[countryIndex]
            let phoneNumber = contact.contactPhone
            let newPhoneNumber = phoneNumber.replacingOccurrences(of: currentCountry.dial_code, with: country.dial_code)
            contactPhoneTextField.text = newPhoneNumber
        }
        
        if nameFirstShow != countries[indexPath.row].name {
            self.saveContactButton.activated(true)
        } else {
            self.saveContactButton.activated(false)
        }
    }
}

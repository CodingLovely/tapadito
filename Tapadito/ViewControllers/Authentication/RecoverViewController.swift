//
//  RecoverViewController.swift
//  Tapadito
//
//  Created by An Phan on 3/14/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

class RecoverViewController: UIViewController {

    // MARK: - IBOutlet
    @IBOutlet weak var wrappedView: UIView!
    @IBOutlet weak var lineTopView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var restoreButton: UIButton!
    
    @IBOutlet weak var bottomWrappedViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var heightWrappedViewConstraint: NSLayoutConstraint!
    
    // MARK: - Private let/var
    private var yCurrent: CGFloat = 66
    private var heightWrappedView: CGFloat = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        emailTextField.delegate = self
        
        prepareStyle()
        addPanGesture()
        setHeightContainerView()
        addTapView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        emailTextField.becomeFirstResponder()
        
        bottomWrappedViewConstraint.constant = 0
        UIView.animate(withDuration: 0.3) {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        wrappedView.roundCorners(corners: [.topLeft, .topRight], radius: 10.0)
    }
    
    // MARK: - IBAction
    @IBAction func restoreButtonAction(_ sender: UIButton) {
        guard let email = emailTextField.text, !email.isEmpty else {
            NotificationBannerView.appearOn(controller: self, text: "Se le olvidó escribir su dirección de correo electrónico.")
            return
        }
        
        showHUD()
        UserService.shared.forgotPassword(email: email) { (json, error) in
            self.dismissHUD()
            if let error = error {
                NotificationBannerView.appearOn(controller: self, text: error.localizedDescription)
            }
            else {
                self.dismissPage()
            }
        }
    }
    
    @IBAction func textFieldEditingChanged(_ sender: UITextField) {
        
    }
    
    // MARK: - Methods
    @objc func respondToSwipeGesture(gesture: UIGestureRecognizer) {
        dismissPage()
    }
    
    // MARK: - Private Methods
    private func dismissPage() {
        bottomWrappedViewConstraint.constant = -heightWrappedView
        UIView.animate(withDuration: 0.5,
                       animations: {
                        self.view.layoutIfNeeded()
        }) { (finished) in
            self.dismiss(animated: false, completion: nil)
        }
    }
    
    private func prepareStyle() {
        lineTopView.roundify()
        emailTextField.customRoundify(coner: 5)
        restoreButton.roundify()
    }
    
    private func setHeightContainerView() {
        // set yDefault of containerView
        let heightView = view.frame.height
        let heightStatusBar = UIApplication.shared.statusBarFrame.height
        heightWrappedView = heightView - heightStatusBar - yCurrent
        heightWrappedViewConstraint?.constant = heightWrappedView
        bottomWrappedViewConstraint?.constant = -heightWrappedView
    }
    
    private func addTapView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        tap.delegate = self
        tap.cancelsTouchesInView = true
        view.addGestureRecognizer(tap)
    }
    
    private func addPanGesture() {
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(respondToSwipeGesture))
        swipeDown.direction = .down
        wrappedView.addGestureRecognizer(swipeDown)
    }
}

// MARK: - UIGestureRecognizerDelegate
extension RecoverViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                           shouldReceive touch: UITouch) -> Bool {
        if let view = touch.view, view.isDescendant(of: wrappedView) {
            return false
        }
        return true
    }
}

// MARK: - UITextFieldDelegate
extension RecoverViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (emailTextField.returnKeyType == UIReturnKeyType.go) {
            restoreButtonAction(restoreButton)
        }
        return false
    }
}

//
//  SignInViewController.swift
//  Tapadito
//
//  Created by An Phan on 3/14/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {

    // MARK: - IBOutlet
    @IBOutlet weak var wrappedView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    
    var signInAction: (() -> Void)?
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        prepareStyle()
        addTapView()
        emailTextField.becomeFirstResponder()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "presentSignUpVCSegue", let signUpVC = segue.destination as? SignUpViewController {
            signUpVC.signUpAction = {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    // MARK: - Methods
    @objc func tapRootView() {
        view.endEditing(true)
    }
    
    // MARK: - Prepare
    func prepareStyle() {
        wrappedView.customRoundify(coner: 7)
        wrappedView.layer.dropShadow()
        
        signInButton.roundify()
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
    }
    
    // MARK: - IBAction
    @IBAction func singInButtonAction(_ sender: UIButton) {
        guard let email = emailTextField.text, !email.isEmpty else {
            NotificationBannerView.appearOn(controller: self, text: "El correo electrónico no puede estar en blanco!")
            return
        }
        guard let password = passwordTextField.text, !password.isEmpty else {
            NotificationBannerView.appearOn(controller: self, text: "La contraseña no puede estar en blanco!")
            
            return
        }
        
        showHUD()
        UserService.shared.login(email: email, password: password, onSuccess: { (user) in
            self.dismissHUD()
            
            AppState.setHome()
            self.dismiss(animated: true, completion: nil)
            self.signInAction?()
        }) { (error) in
            debugPrint(error.localizedDescription)
            NotificationBannerView.appearOn(controller: self, text: error.localizedDescription)
            self.dismissHUD()
        }
    }
    
    @IBAction func signUpButtonAction(_ sender: UIButton) {
        if let _ = presentingViewController as? SignUpViewController {
            dismiss(animated: true, completion: nil)
        }
        else {
            performSegue(withIdentifier: "presentSignUpVCSegue", sender: nil)
        }
    }
    
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        
        UIView.setAnimationsEnabled(false)
        
        if let email = emailTextField.text,
            let password = passwordTextField.text {
            DispatchQueue.main.async {
                self.signInButton.activated(!email.isEmpty && !password.isEmpty)
            }
        }
    }
    
    // MARK: - Private Methods
    private func addTapView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapRootView))
        tap.cancelsTouchesInView = true
        view.addGestureRecognizer(tap)
    }
    
}

// MARK: - UITextFieldDelegate
extension SignInViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        if textField == emailTextField {
            textField.resignFirstResponder()//
            passwordTextField.becomeFirstResponder()//TF2 will respond immediately after TF1 resign.
        } else if (passwordTextField.returnKeyType == UIReturnKeyType.go){
            guard let email = emailTextField.text, !email.isEmpty else {
                NotificationBannerView.appearOn(controller: self, text: "El correo electrónico no puede estar en blanco!")
                return false
            }
            guard let password = passwordTextField.text, !password.isEmpty else {
                NotificationBannerView.appearOn(controller: self, text: "La contraseña no puede estar en blanco!")
                
                return false
            }
            showHUD()
            UserService.shared.login(email: email, password: password, onSuccess: { (user) in
                self.dismissHUD()
                AppState.setHome()
                self.dismiss(animated: true, completion: nil)
                self.signInAction?()
            }) { (error) in
                debugPrint(error.localizedDescription)
                self.dismissHUD()
            }
        }
        return true
        
    }
}

//
//  SignUpViewController.swift
//  Tapadito
//
//  Created by An Phan on 3/14/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit
import SafariServices
import Alamofire
import SwiftyStoreKit
import StoreKit

class SignUpViewController: UIViewController {

    // MARK: - IBOutlet
    @IBOutlet weak var wrappedView: UIView!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var termButton: UIButton!
    @IBOutlet weak var priceCheckImageView: UIImageView!
    
    // MARK: - Variables
    var signUpAction: (() -> Void)?
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationController?.setNavigationBarHidden(true, animated: false)
        prepareStyle()
        addTapView()
        fetchProducts()
        termButton.isSelected = false
        termButton.setImage(UIImage(named: "uncheck-circle"), for: .normal)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        emailTextField.becomeFirstResponder()
        passwordTextField.resignFirstResponder()
        confirmPasswordTextField.resignFirstResponder()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "presentSignInVCSegue",
            let signInVC = segue.destination as? SignInViewController {
            signInVC.signInAction = {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }
    
    // MARK: - IBAction
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
    }
    
    @IBAction func signUpButtonAction(_ sender: UIButton?) {
        guard let email = emailTextField.text, !email.isEmpty else {
            NotificationBannerView.appearOn(controller: self, text: "El correo electrónico no puede estar en blanco!")
            return 
        }
        
        guard let validEmail = emailTextField.text, isValidEmail(email: validEmail) == true else {
            NotificationBannerView.appearOn(controller: self, text: "Email inválido")
            return
        }
        
        guard let password = passwordTextField.text, !password.isEmpty else {
            NotificationBannerView.appearOn(controller: self, text: "La contraseña no puede estar en blanco!")
            
            return
        }
        
        guard let confirmPwd = confirmPasswordTextField.text else { return }
        
        if confirmPwd.isEmpty {
            NotificationBannerView.appearOn(controller: self, text: "Confirmar no puede estar en blanco!")
            return
        }
        else {
            if confirmPwd != password {
                NotificationBannerView.appearOn(controller: self, text: "Esa contraseña no coincide. Inténtalo de nuevo")
                return
            }
        }
        
        if termButton.isSelected == true {
            showHUD()
            UserService.shared.signup(email: email,
                                      password: password,
                                      onSuccess: { (user) in
                Thread.runInMain {
                    self.dismissHUD()
                    DataManager.shared.firstSignUp = true
                    AppState.setHome()
                    self.dismiss(animated: false, completion: nil)
                    self.signUpAction?()
                    
                    delay(1, closure: {
                        var userInfo = [String: Any]()
                        if let product = self.selectedProduct {
                            userInfo["package"] = product
                        }
                        
                        NotificationCenter.default.post(name: Notification.Name.Notify.SignUpNewUser, object: nil, userInfo: userInfo)
                    })
                }
            }) { (error) in
                self.dismissHUD()
                NotificationBannerView.appearOn(controller: self,
                                                text: error.localizedDescription)
            }
        } else {
            NotificationBannerView.appearOn(controller: self, text: "Por favor haga clic en acordar términos y condiciones")
        }
        
    }
    
    @IBAction func viewPriceButtonAction(_ sender: UIButton) {
        showPriceInSafari(for: "https://tapadito.com/pricing.html")
    }
    
    @IBAction func signInButtonAction(_ sender: UIButton) {
        if let _ = presentingViewController as? SignInViewController {
            dismiss(animated: true, completion: nil)
        }
        else {
            performSegue(withIdentifier: "presentSignInVCSegue", sender: nil)
        }
    }
    
    @IBAction func termButtonAction(_ sender: UIButton) {
        termButton.isSelected = !termButton.isSelected
        termButton.setImage(UIImage(named: "agree-icon"), for: .selected)
    }
    
    @IBAction func termAndConditionAction(_ sender: UIButton) {
        
    }
    
    // MARK: - Methods
    @objc func tapRootView() {
        view.endEditing(true)
    }
    
    // MARK: - Private Variable
    private var selectedProduct: SKProduct?
    private func fetchProducts() {
        SwiftyStoreKit.retrieveProductsInfo(Constants.productIds) { (result) in
            let products = Array(result.retrievedProducts.sorted(by: { $0.price.doubleValue < $1.price.doubleValue }))
            self.selectedProduct = products.first
            self.priceCheckImageView.isHidden = self.selectedProduct == nil
            if let product = products.first {
                self.priceLabel.text = "Tapadito sólo cuesta \(product.localizedPrice!)."
            }
            else {
                self.priceLabel.text = "Tapadito sólo cuesta $3.99"
            }
        }
    }
    
    private func addTapView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapRootView))
        tap.cancelsTouchesInView = true
        view.addGestureRecognizer(tap)
    }
    
    private func showPriceInSafari(for url: String) {
        guard let url = URL(string: url) else {
            return
        }
        let safariVC = SFSafariViewController(url: url)
        present(safariVC, animated: true)
    }
    
    private func prepareStyle() {
        
        emailTextField.delegate = self
        passwordTextField.delegate = self
        confirmPasswordTextField.delegate = self
        
        wrappedView.customRoundify(coner: 5)
        wrappedView.layer.dropShadow()
        
        emailTextField.customRoundify(coner: 5)
        passwordTextField.customRoundify(coner: 5)
        confirmPasswordTextField.customRoundify(coner: 5)
        
        signUpButton.roundify()
    }
    
    private func isValidEmail(email:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let checkValid = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return checkValid.evaluate(with: email)
    }
}

// MARK: - UITextFieldDelegate
extension SignUpViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        if textField == emailTextField {
            textField.resignFirstResponder()
            passwordTextField.becomeFirstResponder()//TF2 will respond immediately after TF1 resign.
        } else if textField == passwordTextField  {
            textField.resignFirstResponder()
            confirmPasswordTextField.becomeFirstResponder()//TF3 will respond first
        } else if( confirmPasswordTextField.returnKeyType == UIReturnKeyType.go) {
            self.signUpButtonAction(signUpButton)
        }
        
        return true
    }
}

//
//  LandingViewController.swift
//  Tapadito
//
//  Created by An Phan on 3/14/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit

class LandingViewController: UIViewController {

    // MARK: - IBOutlets
    @IBOutlet weak var videoBackgroundView: UIView!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var skipButton: UIButton!
    
    // MARK: - Variable
    var player: AVPlayer?
    let videoURL = URL(string:  WebUrl.landingVideo)!

    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        translucentNavigationBar()
        settupVideoView()
        
        signInButton.roundify()
        signUpButton.roundify()
        skipButton.roundify()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        settupVideoView()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "presentSignInSegue", let signVC = segue.destination as? SignInViewController {
            signVC.signInAction = {
                self.dismiss(animated: false, completion: nil)
            }
        }
        else if segue.identifier == "presentSignUpSegue", let signVC = segue.destination as? SignUpViewController {
            signVC.signUpAction = {
                self.dismiss(animated: false, completion: nil)
            }
        }
    }

    // MARK: - IBAction
    @IBAction func skipButtonAction(_ sender: Any) {
        NotificationCenter.default.post(name: Notification.Name.Notify.sessionChanged, object: nil)
        AppState.setWelcome()
        dismiss(animated: false, completion: nil)
    }
    
    // MARK: - Methods
    func settupVideoView(){
         let url = Bundle.main.url(forResource:"tapadito-landing", withExtension: "MP4")
        player = AVPlayer(url: url!)
        player?.actionAtItemEnd = .none
        player?.isMuted = true

        let playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        playerLayer.zPosition = -1
        playerLayer.frame = videoBackgroundView.bounds
        videoBackgroundView.layer.addSublayer(playerLayer)
        player?.play()
        
        // add observer to watch for video end in order to loop video
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: self.player?.currentItem, queue: .main) { _ in
            self.player?.seek(to: CMTime.zero);
            self.player?.play()
            
        }
    }
}

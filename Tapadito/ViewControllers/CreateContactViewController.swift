//
//  ContactDetailViewController.swift
//  Tapadito
//
//  Created by An Phan  on 3/26/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

class CreateContactViewController: UIViewController {
    
    // MARK: - Variable
    var contact: Contact!
    var countries = [Country]()
    var country: Country!
    var nameFirstShow: String!
    var tempNum: String!
    var tempCode: String!
    var tempDialCode: String!
    var newDialCode: String!
    var saveContactAction: (() -> Void)?
    
    private var selectedCountry: Country?
    
    // MARK: - Private var/let
    private var heightRow: CGFloat = 41
    private var widthCountryViewList: CGFloat = 0
    private var heightCountryViewList: CGFloat = 0
    private var spacing: CGFloat = 15
    
    // MARK: - IBOutlet
    @IBOutlet weak var contactNameTextField: UITextField!
    
    @IBOutlet weak var wrappedPhoneView: UIView!
    @IBOutlet weak var dialCodeTextField: UITextField!
    @IBOutlet weak var contactPhoneTextField: UITextField!
    @IBOutlet weak var saveContactButton: UIButton!
    
    @IBOutlet weak var wrappedTableView: UIView!
    @IBOutlet weak var countrySelectButton: UIButton!
    @IBOutlet weak var wrappedSelectView: UIView!
    
    @IBOutlet weak var nameSelectLabel: UILabel!
    @IBOutlet weak var codeSelectLabel: UILabel!
    
    @IBOutlet weak var tableViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Nuevo contacto"
        
        settupUI()
        
        loadJson(fileName: "countries")
        
        prepareStyle()
        defaultFrame()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        nameFirstShow = nameSelectLabel.text!
        tableViewHeightConstraint.constant = (heightCountryViewList * 9.5) / 10
        tableViewWidthConstraint.constant = (widthCountryViewList * 9.5) / 10
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        tapRootView()
        
        wrappedTableView.alpha = 0
        wrappedTableView.isHidden = true
        countrySelectButton.isSelected = false
    }
    
    // MARK: - IBActions
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        UIView.setAnimationsEnabled(false)
        if let phoneNumber = contactPhoneTextField.text {
            self.saveContactButton.activated(!phoneNumber.isEmpty)
        }
    }
    
    @IBAction func selectCountryAction(_ sender: UIButton) {
        tapRootView()
        
        let width = sender.isSelected ? (widthCountryViewList * 9.5) / 10 : widthCountryViewList
        let height = sender.isSelected ? (heightCountryViewList * 9.5) / 10 : heightCountryViewList
        wrappedTableView.alpha = sender.isSelected ? 0 : 1
        wrappedTableView.isHidden = sender.isSelected
        tableViewWidthConstraint.constant = width
        tableViewHeightConstraint.constant = height
        UIView.animate(withDuration: 0.5) {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
        sender.isSelected = !sender.isSelected
    }
    
    @IBAction func saveContactAction(_ sender: UIButton) {
        guard let name = contactNameTextField.text, !name.isEmpty else {
            NotificationBannerView.appearOn(controller: self, text: "El nombre no puede estar en blanco!")
            return
        }
        
        guard let dialCode = dialCodeTextField.text, !name.isEmpty else {
            NotificationBannerView.appearOn(controller: self, text: "El código de marcación no puede estar en blanco!")
            return
        }
        
        guard let phone = contactPhoneTextField.text, !name.isEmpty else {
            NotificationBannerView.appearOn(controller: self, text: "El teléfono no puede estar en blanco!")
            return
        }
        
        guard let country = selectedCountry else {
            NotificationBannerView.appearOn(controller: self, text: "El código de marcación no puede estar en blanco!")
            return
        }
        
        let tempPhone = phone.removeTheSpecialCharAtFirst(char: "0")
        
        let contact = Contact(name: name, phone: "\(dialCode)\(tempPhone)", country: country.code)
        
        showHUD()
        ContactService.shared.addContact(contact: contact, onSuccess: { (contact) in
            self.dismissHUD()
            if let navController = self.navigationController {
                navController.popViewController(animated: true, completion: {
                    self.saveContactAction?()
                })
            }
        }) { (error) in
            self.dismissHUD()
            debugPrint(error.localizedDescription)
            DispatchQueue.main.async {
                NotificationBannerView.appearOn(controller: self, text: error.localizedDescription)
            }
        }
    }
    
    // MARK: - Private Methods
    private func prepareStyle(){
        saveContactButton.customRoundify(coner: 7)
        
        wrappedTableView.bordered(withColor: "#c3c3c3", width: 0.3, radius: 5)
        wrappedTableView.layer.applySketchShadow()
        
        wrappedSelectView.bordered(withColor: "#c3c3c3", width: 0.3, radius: 5)
        wrappedPhoneView.bordered(withColor: "#c3c3c3", width: 0.3, radius: 5)
        
        if let country = selectedCountry {
            nameSelectLabel.text = country.name
            codeSelectLabel.text = country.code
            dialCodeTextField.text = country.dial_code
        }
    }
    
    private func tapRootView() {
        view.endEditing(true)
    }
    
    private func loadJson(fileName: String) {
        if let filePath = Bundle.main.path(forResource: fileName, ofType: "json"), let data = NSData(contentsOfFile: filePath) {
            do {
                let decoder = JSONDecoder()
                self.countries = try decoder.decode([Country].self, from: data as Data)
                self.selectedCountry = countries.first
            } catch _ {
                // Doing something
            }
        }
    }
    
    private func settupUI(){
        contactNameTextField.delegate = self
        contactPhoneTextField.delegate = self
        
        contactNameTextField.resignFirstResponder()
        contactPhoneTextField.resignFirstResponder()
        
        saveContactButton.activated(false)
        wrappedTableView.isHidden = true
        countrySelectButton.isSelected = false
        wrappedTableView.alpha = 0
    }
    
    private func defaultFrame() {
        // frame relationship list
        widthCountryViewList = view.frame.width - spacing * 2
        heightCountryViewList = heightRow * 4 - 1
    }
}

// MARK: - UITextFieldDelegate
extension CreateContactViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == contactNameTextField {
            contactPhoneTextField.becomeFirstResponder()
        }
        else {
            textField.resignFirstResponder()
        }
        
        return true
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if countrySelectButton.isSelected == true && wrappedTableView.isHidden == false {
            countrySelectButton.isSelected = !countrySelectButton.isSelected
            wrappedTableView.isHidden = !countrySelectButton.isSelected
        }
        
        UIView.animate(withDuration: 0.5) {
            self.wrappedTableView.layoutIfNeeded()
        }
        
        return true
    }
}

// MARK: - UITableViewDataSource
extension CreateContactViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CreateContactTableViewCell", for: indexPath) as! CreateContactTableViewCell
        cell.nameLabel.text = countries[indexPath.row].name
        cell.codeLabel.text = countries[indexPath.row].code
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension CreateContactViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightRow
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        wrappedTableView.isHidden = true
        countrySelectButton.isSelected = false
        
        selectedCountry = countries[indexPath.row]
        
        if let country = selectedCountry {
            nameSelectLabel.text = country.name
            codeSelectLabel.text = country.code
            dialCodeTextField.text = country.dial_code
        }
    }
}

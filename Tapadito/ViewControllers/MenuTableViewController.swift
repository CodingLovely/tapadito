//
//  MenuTableViewController.swift
//  Tapadito
//
//  Created by An Phan  on 3/15/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

class MenuTableViewController: UITableViewController {
    
    // MARK: - Private let/var
    private let items = ["Bienvenido", "Entrar"]
    private let sessionItems = ["Mi Informacion", "Mis Contactos", "Mis Mensajes", "Enviar Mensaje", "Salir"]
    
    private let guestVCIDs = ["welcomeNCID", "featuresNCID"]
    private let sessionVCIDs  = [ "homeNCID", "contactNCID", "messagesNCID", "sendMessageNCID"]
    
    // MARK: - IBOutlet
    @IBOutlet weak var logoLabel: UILabel!
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareStyle()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reload),
                                               name: Notification.Name.Notify.sessionChanged,
                                               object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    // MARK: - Methods
    @objc func reload() {
        tableView.reloadData()
    }
   
    // MARK: - Private Methods
    private func prepareStyle() {
        logoLabel.customRoundify(coner: 7)
    }
    
    fileprivate func showSignOutAlert() {
        let actionSheet = UIAlertController(title: "Estás seguro de que quieres cerrar sesión?", message: nil, preferredStyle: .actionSheet)
        
        let yesAction = UIAlertAction(title: "Sí", style: .destructive) { (alertAction) -> Void in
            AppState.logout()
            AppState.clearSessionUser()
            AppState.setWelcome()
            self.reload()
        }
        
        let noAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        
        actionSheet.addAction(yesAction)
        actionSheet.addAction(noAction)
        
        DispatchQueue.main.async {
            UIApplication.topViewController()?.present(actionSheet,
                                                       animated: true,
                                                       completion: nil)
        }
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return AppState.UserAuth.isAuthenticated() ? sessionItems.count : items.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MenuTableViewCell", for: indexPath) as! MenuTableViewCell
        let item = AppState.UserAuth.isAuthenticated() ? sessionItems[indexPath.row] : items[indexPath.row]
        cell.itemMenuNameLabel.text = item
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
    
    // MARK: - Table view delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
            
        case 0:
            let vcIdentifier = AppState.UserAuth.isAuthenticated() ? sessionVCIDs[0] : "welcomeNCID"
            let keyboardNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: vcIdentifier) as! UINavigationController
            sideMenuController?.embed(centerViewController: keyboardNC, cacheIdentifier: nil)
            
        case 1:
            if AppState.UserAuth.isAuthenticated() {
                let keyboardNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: sessionVCIDs[1]) as! UINavigationController
                sideMenuController?.embed(centerViewController: keyboardNC, cacheIdentifier: nil)
            }
            else {
                AppState.presentLogin()
            }
        case 2:
            if AppState.UserAuth.isAuthenticated() {
                let vcIdentifier = AppState.UserAuth.isAuthenticated() ? sessionVCIDs[2] : "webNCID"
                let keyboardNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: vcIdentifier) as! UINavigationController
                if !AppState.UserAuth.isAuthenticated() {
                    if let webVC = keyboardNC.topViewController as? WebViewController {
                        webVC.getDataURL = WebUrl.features
                    }
                }
                
                sideMenuController?.embed(centerViewController: keyboardNC, cacheIdentifier: nil)
            }
        case 3:
            if AppState.UserAuth.isAuthenticated() {
                let identifier = DataManager.shared.currentUser.credits > 0 ? sessionVCIDs[3] : "homeNCID"
                let keyboardNC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: identifier) as! UINavigationController
                sideMenuController?.embed(centerViewController: keyboardNC, cacheIdentifier: nil)
            }
            else {
                AppState.presentLogin()
            }
            
        case 4:
            if AppState.UserAuth.isAuthenticated() {
                showSignOutAlert()
            }
            
        default:
            break
        }
    }
}

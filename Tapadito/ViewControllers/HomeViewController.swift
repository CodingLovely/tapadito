//
//  HomeViewController.swift
//  Tapadito
//
//  Created by An Phan  on 3/17/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit
import SwiftyStoreKit
import StoreKit

class HomeViewController: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var logoLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var creditLabel: UILabel!
    @IBOutlet weak var dateLicenseLabel: UILabel!
    @IBOutlet weak var upgrateButton: UIButton!
    @IBOutlet weak var arrowRightImage: UIImageView!
    @IBOutlet weak var contentViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Variables
    var user: User!
    var paidPayment: Payment?
    var countries = [Country]()
    var tempDielCode: String!
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareStyle()
        fetchUserInfo()
        loadJson(fileName: "countries")
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reload),
                                               name: Notification.Name.Notify.sessionChanged,
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(makePayment),
                                               name: Notification.Name.Notify.SignUpNewUser,
                                               object: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showNewPwdPageSegue" {
            let vc = segue.destination as! UpdatePwdViewController
            
            // Handle change password
            vc.changePwdButtonAction = {
                 NotificationBannerView.appearOn(controller: self, text: "La nueva contraseña no debe estar vacía")
            }
            
            vc.newPwdAction = {
                print(vc.newPwd)
            }
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - Methods
    @objc func reload() {
        if AppState.UserAuth.isAuthenticated() {
            user = DataManager.shared.currentUser
            
            renderData()
        }
    }
    
    @objc func makePayment(notification: Notification) {
        if let product = notification.userInfo?["package"] as? SKProduct,
            let credits = getCreditsById(productId: product.productIdentifier) {
            showHUD()
            PaymentService.shared.createPayment(credits: credits,
                                                amount: Int(truncating: product.price),
                                                onSuccess: { (payment) in
                self.paidPayment = payment
                // Handle buy product
                SwiftyStoreKit.purchaseProduct(product.productIdentifier, completion: { (result) in
                    switch result {
                    case .success:
                        PaymentService.shared.capturePayment(payment: payment,
                                                             onSuccess: { (user) in
                            self.dismissHUD()
                            NotificationBannerView.appearOn(controller: self,
                                                            text: "Tu cuenta está actualizada!",
                                                            animateTime: .middle)
                            self.fetchUserInfo()
                        }) { (error) in
                            self.dismissHUD()
                            self.promptPaying()
                        }
                    case .error(let error):
                        self.promptPaying()
                        self.dismissHUD()
                        NotificationBannerView.appearOn(controller: self,
                                                        text: error.localizedDescription,
                                                        animateTime: .middle)
                    }
                })
            }) { (error) in
                self.dismissHUD()
                self.promptPaying()
            }
        }
    }
    
    // MARK: - Private methods
    fileprivate func getCreditsById(productId: String) -> Int? {
        let components = productId.components(separatedBy: ".")
        if let lastComp = components.last, !lastComp.isEmpty {
            return Int(lastComp)
        }
        
        return nil
    }
    
    private func loadJson(fileName: String) {
        if let filePath = Bundle.main.path(forResource: fileName, ofType: "json"), let data = NSData(contentsOfFile: filePath) {
            do {
                let decoder = JSONDecoder()
                self.countries = try decoder.decode([Country].self, from: data as Data)
                
            } catch _ {
                // Doing something
            }
        }
    }
    
    private func fetchUserInfo() {
        if AppState.UserAuth.isAuthenticated() {
            showHUD()
            UserService.shared.fetchUserInfo(onSuccess: { (user) in
                self.dismissHUD()
                self.user = user
                self.renderData()
                if DataManager.shared.firstSignUp {
                    DataManager.shared.firstSignUp = false
                }
                else {
                    self.promptPaying()
                }
            }) { (error) in
                self.dismissHUD()
            }
        }
    }
    
    private func promptPaying() {
        if let user = DataManager.shared.currentUser, user.isNewphone == 0 {
            let creditVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreditVC") as! CreditViewController
            let creditNC = UINavigationController(rootViewController: creditVC)
            present(creditNC, animated: false, completion: nil)
            NotificationBannerView.appearOn(controller: creditVC, text: "Actualice su cuenta para usar todas las funciones de Tapadito", animateTime: .slow, isNavi: true)
        }
    }
    
    private func prepareStyle() {
        logoLabel.customRoundify(coner: 12)
        upgrateButton.customRoundify(coner: 10)
        arrowRightImage.tintColor = UIColor(hexString: "0090C5")
    }
    
    private func renderData() {
        
        let currentCountry = user.country
        let currentPhone = user.phone
        guard let countryIndex = countries.firstIndex(where: { $0.code == currentCountry }) else {
          return
        }
        let currentDialCode = countries[countryIndex].dial_code
        var parsed = currentPhone.replacingOccurrences(of: currentDialCode, with: "")
        let count = parsed.count
        for i in  0..<count {
            if i == 3 || i == 7{
                 parsed.insert("-", at: parsed.index(parsed.startIndex, offsetBy: i))
            }
        }
        phoneNumberLabel.text = "\(currentDialCode)-\(parsed)"
        creditLabel.text = "\(user.credits)"

        if let diff = Calendar.current.dateComponents([.day], from: user.lastPaymentDate, to: Date()).day {
            let remainingDay = (30 - diff) > 0 ? 30 - diff : 0
            dateLicenseLabel.text = "\(remainingDay)"
        }
    }
    
    // MARK: - IBAction
    @IBAction func changePassword(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3) {
            self.performSegue(withIdentifier: "showNewPwdPageSegue", sender: nil)
        }
    }
}

//
//  ContactDetailViewController.swift
//  Tapadito
//
//  Created by An Phan  on 3/16/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit
import Contacts
import Alamofire

class MessageDetailViewController: UIViewController {

    var contact: Contact!
    let msgInputView: MsgInputView = MsgInputView.fromNib()
    
    var contactsPhone = [CNContact]()
    var tempNumber: String?
    var tempPhoneModel = [ContactFromPhone]()
    var countryCode: String = "+1"
    
    // MARK: - Private Variable
    
    private var messages = [Message]()
    private var countries = [Country]()
    private var country: Country!
    private var refreshTimer: Timer?
    
    // MARK: - IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        prepareTitle()
        
        tableView.keyboardDismissMode = .onDrag
        addTapView()
        tapRootView()
        handleBuyCredit()
        loadJson(fileName: "countries")
        handleSendMessage()
        
        showHUD()
        fetchMessageDetail()
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardDidHide), name: UIResponder.keyboardDidHideNotification, object: nil)
        
        refreshTimer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true, block: { (timer) in
            self.fetchMessageDetail()
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Show/hide buy button
        let creditCount = DataManager.shared.currentUser.credits
        msgInputView.bottomBgView.isHidden = creditCount == 0
        msgInputView.buyCreditButton.isHidden = creditCount > 0
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        refreshTimer?.invalidate()
        refreshTimer = nil
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tableView.scrollToBottom()
    }
    
    override var inputAccessoryView: UIView? {
        let bottomSpacing: CGFloat = UIScreen.main.nativeBounds.height >= 812 ? 34 : 10
        let cmtInputViewHeight = msgInputView.bottomBGViewHeightConstraint.constant + bottomSpacing
        msgInputView.frame.size.height = cmtInputViewHeight
        
        return msgInputView
    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    // MARK: - Methods
    @objc func tapRootView() {
        view.endEditing(true)
    }
    
    @objc func keyboardWillShow(notification: Notification) {
        if let keyboardSize = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: keyboardSize.height, right: 0)
        }
    }
    
    @objc func keyboardDidHide(notification: Notification) {
        tableView.contentInset = UIEdgeInsets.zero
    }

    // MARK: - Private methods
    private func loadJson(fileName: String) {
        if let filePath = Bundle.main.path(forResource: fileName, ofType: "json"), let data = NSData(contentsOfFile: filePath) {
            do {
                let decoder = JSONDecoder()
                self.countries = try decoder.decode([Country].self, from: data as Data)
                if let countryIndex = countries.firstIndex(where: { $0.code == contact.country }) {
                    self.country = self.countries[countryIndex]
                }
            } catch _ {
                // Doing something
            }
        }
    }
    
    private func addTapView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapRootView))
        tap.cancelsTouchesInView = true
        view.addGestureRecognizer(tap)
    }
    
    @objc func fetchMessageDetail() {
        debugPrint("fetchMessageDetail")
        MessageService.shared.messageDetail(contact: contact, onSuccess: { (messages) in
            self.dismissHUD()
            self.messages = messages
            self.tableView.reloadData()
        }) { (error) in
            self.dismissHUD()
            debugPrint(error.localizedDescription)
        }
    }
    
    private func handleBuyCredit() {
        msgInputView.buyButtonAction = {
            let creditVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreditVC")
            
            self.navigationController?.pushViewController(creditVC, animated: true)
        }
    }
    
    private func handleSendMessage() {
        msgInputView.sendButtonAction = {
            self.showHUD()
            MessageService.shared.sendMessages(country: self.country, phoneNumber: self.contact.contactPhone, message: self.msgInputView.textView.text, onSuccess: { (message) in
                self.dismissHUD()
                self.messages.append(message)
                self.msgInputView.textViewEmpty()
                self.tableView.reloadData()
                self.tableView.scrollToBottom()
            }, onError: { (error) in
                self.dismissHUD()
                NotificationBannerView.appearOn(controller: self, text: error.localizedDescription)
            })
        }
    }
    
    private func prepareTitle(){
        Alamofire.request(WebApi.ipDetect).responseJSON{ response in
            if let locationJSON = response.result.value{
                let locationObject: Dictionary = locationJSON as! Dictionary<String, Any>
                let locations = locationObject["location"]! as! [String: Any]
                self.countryCode = locations["calling_code"]! as! String
                self.getPhoneContacts()
            }
        }
    }
    
    private func getPhoneContacts() {
        let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
        let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
        do {
            try CNContactStore().enumerateContacts(with: request) { (contact, stop) in
                self.contactsPhone.append(contact)
            }
        } catch {
            print(error.localizedDescription)
        }
        listContactFromPhone()
        navigationTitle()
    }
    
    private func listContactFromPhone() {
        for i in 0..<contactsPhone.count{
            for phoneNumber in contactsPhone[i].phoneNumbers {
                guard let number = phoneNumber.value as? CNPhoneNumber else {
                    return
                }
                self.tempNumber = number.stringValue
            }
            tempPhoneModel.append(ContactFromPhone(name: "\(contactsPhone[i].givenName) \(contactsPhone[i].familyName)", number: tempNumber!))
        }
    }
    
    private func removeCharactor( number: String ) -> String {
        let chars: [Character] = [")", "(", "-", " "]
        let phoneText = number.stringByRemovingAll(characters: chars)
        return phoneText
    }
    
    private func navigationTitle() {
        let matchedContacts = tempPhoneModel.filter({
            var tmpPhone = "\(removeCharactor(number: $0.number))"
            if !tmpPhone.hasPrefix("+") {
                tmpPhone = "+\(countryCode)\(removeCharactor(number: $0.number))"
            }
            
            return tmpPhone == contact.contactPhone })
        
        let contactName = contact.contactName.trimmingCharacters(in: .whitespacesAndNewlines)
        if let firstContact = matchedContacts.first {
            var tmpPhone = "\(removeCharactor(number: firstContact.number))"
            if !tmpPhone.hasPrefix("+") {
                tmpPhone = "+\(countryCode)\(removeCharactor(number: firstContact.number))"
            }
            
            if contactName == contact.contactPhone && tmpPhone == contact.contactPhone {
                title = "Conversación con: \(firstContact.name)"
            }
            else {
                title = "Conversación con: \(contact.contactName)"
            }
        }
        else {
            title = "Conversación con: \(contact.contactName)"
        }
    }
}

// MARK: - UITableViewDataSource
extension MessageDetailViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MessageDetailTableViewCell", for: indexPath) as! MessageDetailTableViewCell
        let message = messages[indexPath.row]
        cell.renderMessageDetail(message: message)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

}

// MARK: - UITableViewDelegate
extension MessageDetailViewController: UITableViewDelegate {
   
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}

// MARK: - UIScrollViewDelegate
extension MessageDetailViewController: UIScrollViewDelegate {
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}

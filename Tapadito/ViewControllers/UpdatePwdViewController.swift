//
//  UpdatePwdViewController.swift
//  Tapadito
//
//  Created by An Phan  on 3/24/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

class UpdatePwdViewController: UIViewController {

    // MARK: - IBOutlet
    @IBOutlet weak var wrappedView: UIView!
    @IBOutlet weak var newPwdTextField: UITextField!
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var changeButton: UIButton!
    
    // MARK: - Variables
    var changePwdButtonAction: (() -> Void)?
    var newPwdAction: (() -> Void)?
    var newPwd: String!
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        prepareStyle()
        addTapView()
        
        newPwdTextField.becomeFirstResponder()
        newPwdTextField.delegate = self
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        view.endEditing(true)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        tapRootView()
    }
    
    // MARK: - Prepare Style
    private func prepareStyle() {
        wrappedView.customRoundify(coner: 10)
        cancelButton.customRoundify(coner: 7)
        changeButton.customRoundify(coner: 7)
    }
    
    // MARK: - IBAction
    @IBAction func cancelAction(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func changeAction(_ sender: UIButton) {
        guard let newPwd = newPwdTextField.text, !newPwd.isEmpty else {
            NotificationBannerView.appearOn(controller: self, text: "La nueva contraseña no debe estar vacía")
            return
        }
        
        UserService.shared.newPassword(newPassword: newPwd) { (json, error) in
            if let error = error {
                NotificationBannerView.appearOn(controller: self, text: error.localizedDescription)
            }
            else {
                self.dismiss(animated: false) {
                    self.changePwdButtonAction?()
                }
            }
        }
    }
    
    // MARK: - Methods
    @objc func tapRootView() {
        dismiss(animated: true, completion: nil)
    }
    
    // MARK: - Private Methods
    private func addTapView() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapRootView))
        tap.delegate = self
        view.addGestureRecognizer(tap)
    }
}

// MARK: - UITextFieldDelegate
extension UpdatePwdViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if (newPwdTextField.returnKeyType == UIReturnKeyType.send) {
            guard let newPwd = newPwdTextField.text, !newPwd.isEmpty else {
                NotificationBannerView.appearOn(controller: self, text: "La nueva contraseña no debe estar vacía")
                return false
            }
            self.dismiss(animated: false) {
                self.changePwdButtonAction?()
                self.dismiss(animated: true, completion: nil)
            }
            return true;
        }
        
        return false;
    }
}

// MARK: - UIGestureRecognizerDelegate
extension UpdatePwdViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer,
                           shouldReceive touch: UITouch) -> Bool {
        if let view = touch.view, view.isDescendant(of: wrappedView) {
            return false
        }
        return true
    }
}

//
//  SendMessageViewController.swift
//  Tapadito
//
//  Created by An Phan on 3/13/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

class SendMessageViewController: UIViewController {

    // MARK: - IBOutlet
    @IBOutlet weak var countryTableView: UITableView!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var nameCountrySelectLabel: UILabel!
    @IBOutlet weak var codeCountrySelectLabel: UILabel!
    @IBOutlet weak var countrySelectButton: UIButton!
    @IBOutlet weak var wrappedTableView: UIView!
    @IBOutlet weak var wrappedSelectView: UIView!
    @IBOutlet weak var dialCodeTextField: UITextField!
    @IBOutlet weak var wrappedPhoneLabel: UIView!
    @IBOutlet weak var tableViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var tableViewHeightConstraint: NSLayoutConstraint!
    
    // MARK: - Private var/let
    private var countries = [Country]()
    private var country: Country!
    
    private var heightRow: CGFloat = 41
    private var widthCountryViewList: CGFloat = 0
    private var heightCountryViewList: CGFloat = 0
    private var spacing: CGFloat = 15
    private var phoneText = ""
    private var messageText = ""
    
    var phone: String?
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Enviar Mensaje"
       
        loadJson(fileName: "countries")
        
        if let phone = phone {
            phoneText = phone
            
            if phone.hasPrefix("+") {
                for countryCode in countries {
                    if phone.hasPrefix(countryCode.dial_code) {
                        phoneText = phone.replacingOccurrences(of: countryCode.dial_code, with: "")
                        break
                    }
                }
            }
            else {
                phoneText = phone.removeTheSpecialCharAtFirst(char: "0")
            }
            
            phoneNumberTextField.text = phoneText
        }
        
        prepareStyle()
        addObserver()
        defaultFrame()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableViewHeightConstraint.constant = (heightCountryViewList * 9.5) / 10
        tableViewWidthConstraint.constant = (widthCountryViewList * 9.5) / 10
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        tapRootView()
        countrySelectButton.isSelected = false
        wrappedTableView.isHidden = true
        wrappedTableView.alpha = 0
        
        UIView.animate(withDuration: 0.5) {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: - IBAction
    @IBAction func selectCountryAction(_ button: UIButton) {
        tapRootView()
        
        let width = button.isSelected ? (widthCountryViewList * 9.5) / 10 : widthCountryViewList
        let height = button.isSelected ? (heightCountryViewList * 9.5) / 10 : heightCountryViewList
        wrappedTableView.alpha = button.isSelected ? 0 : 1
        wrappedTableView.isHidden = button.isSelected
        tableViewWidthConstraint.constant = width
        tableViewHeightConstraint.constant = height
        UIView.animate(withDuration: 0.5) {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }
        
        button.isSelected = !button.isSelected
    }
    
    @IBAction func sendButtonAction(_ sender: UIButton) {
    
        phoneNumberTextField.becomeFirstResponder()
        contentTextView.text = "Escriba aquí su mensaje"
        contentTextView.textColor = UIColor.lightGray
        phoneNumberTextField.text = ""
        DispatchQueue.main.async {
            self.sendButton.activated(false)
        }
        
        showHUD()
        let tempPhone = phoneText.removeTheSpecialCharAtFirst(char: "0")
        if let country = country {
            let phoneNumber = dialCodeTextField.text! + tempPhone
            print(phoneNumber)
            MessageService.shared.sendMessages(country: country,
                                               phoneNumber: phoneNumber,
                                               message: self.messageText,
                                               onSuccess: { (messages) in
                self.dismissHUD()
                        
            }) { (error) in
                self.dismissHUD()
            }
        }
    }
    
    @IBAction func textFieldEditingChanged(_ textField: UITextField) {
        UIView.setAnimationsEnabled(false)
    
        if let phone = phoneNumberTextField.text {
            self.phoneText = phone
            
            DispatchQueue.main.async {
                self.sendButton.activated(!phone.isEmpty && !self.messageText.isEmpty)
            }
        }
    }
    
    // MARK: - Methods
    @objc func keyboardWillHide(notification: Notification) {
        if ((notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            if self.view.frame.origin.y != 0 {
                self.view.frame.origin.y = 0
            }
        }
    }
    
   // MARK: - Private methods
    private func tapRootView() {
        view.endEditing(true)
    }
    
    private func addObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(SendMessageViewController.keyboardWillHide), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    private func loadJson(fileName: String) {
        if let filePath = Bundle.main.path(forResource: fileName, ofType: "json"), let data = NSData(contentsOfFile: filePath) {
            do {
                let decoder = JSONDecoder()
                self.countries = try decoder.decode([Country].self, from: data as Data)
            } catch _ {
                // Doing something
            }
        }
    }
    
    private func prepareStyle() {
        
        //Text View
        contentTextView.bordered(withColor: "#c3c3c3", width: 0.2, radius: 5)
        contentTextView.text = "Escriba aquí su mensaje"
        contentTextView.textColor = UIColor.lightGray
        contentTextView.delegate = self
        contentTextView.textColor = UIColor.lightGray
        contentTextView.resignFirstResponder()
        
        //TableView
        wrappedTableView.layer.applySketchShadow()
        wrappedTableView.isHidden = true
        wrappedTableView.alpha = 0
        
        //Send Button
        sendButton.customRoundify(coner: 5)
        sendButton.activated(false)
        
        //Phone Number
        wrappedPhoneLabel.bordered(withColor: "#eeeeee", width: 0.3, radius: 5)
        phoneNumberTextField.delegate = self
        phoneNumberTextField.resignFirstResponder()
        
        countryTableView.bordered(withColor: "#c3c3c3", width: 0, radius: 5)
        wrappedSelectView.bordered(withColor: "#c3c3c3", width: 0.3, radius: 5)
        
        //Value of first load tableView cell
        if let firstCountry = countries.first {
            country = firstCountry
            nameCountrySelectLabel.text = firstCountry.name
            codeCountrySelectLabel.text = firstCountry.code
            dialCodeTextField.text = firstCountry.dial_code
        }
    }
    
    private func defaultFrame() {
        // frame relationship list
        widthCountryViewList = view.frame.width - spacing * 2
        heightCountryViewList = heightRow * 4 - 1
    }
}

// MARK: - UITableViewDataSource
extension SendMessageViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SendMessageTableViewCell", for: indexPath) as! SendMessageTableViewCell
        cell.nameLabel.text = countries[indexPath.row].name
        cell.codeLabel.text = countries[indexPath.row].code
        return cell
    }
}

// MARK: - UITableViewDelegate
extension SendMessageViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return heightRow
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        nameCountrySelectLabel.text = countries[indexPath.row].name
        codeCountrySelectLabel.text = countries[indexPath.row].code
        dialCodeTextField.text = countries[indexPath.row].dial_code
        country = countries[indexPath.row]
        
        wrappedTableView.isHidden = true
        wrappedTableView.alpha = 0
        self.countrySelectButton.isSelected = false
        
        UIView.animate(withDuration: 0.5, animations: {
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        })
    }
}

// MARK: - UITextFieldDelegate
extension SendMessageViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
       wrappedTableView.isHidden = true
    }
}

// MARK: - UITextViewDelegate
extension SendMessageViewController: UITextViewDelegate {
    func textViewDidBeginEditing(_ textView: UITextView) {
        wrappedTableView.isHidden = true
        
        UIView.animate(withDuration: 0.25) {
            self.view.frame.origin.y -= 200
            self.view.layoutIfNeeded()
        }
        
        if textView.textColor == UIColor.lightGray {
            contentTextView.text = nil
            contentTextView.textColor = UIColor(hexString: "#444444")
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        UIView.animate(withDuration: 0.25) {
            self.view.frame.origin.y = 0
            self.view.layoutIfNeeded()
        }
        
        if contentTextView.text == "" {
            contentTextView.text = "Escriba aquí su mensaje"
            contentTextView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        self.messageText = textView.text
        DispatchQueue.main.async {
            self.sendButton.activated(!self.phoneText.isEmpty && !self.messageText.isEmpty)
        }
    }
}

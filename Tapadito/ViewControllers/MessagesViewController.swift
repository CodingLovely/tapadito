//
//  MessageViewController.swift
//  Tapadito
//
//  Created by An Phan on 3/13/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit
import Contacts
import Alamofire

class MessageViewController: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var tableView: UITableView!
    
    private var contacts = [Contact]()
    
    var countryCode: String = "+1"
    
    var tempNumber: String?
    var tempPhoneModel = [ContactFromPhone]()
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Mis Mensajes"
        
        prepareTableView()
        showHUD()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Refresh conversation.
        fetchMessages()
    }
    
    // MARK: - Private method
    private func prepareTableView() {
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.contentInset = UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 0)
        tableView.reloadData()
    }
    
    private func fetchMessages() {
        let group = DispatchGroup()
        
        group.enter()
        Alamofire.request(WebApi.ipDetect).responseJSON{ response in
            group.leave()
            if let locationJSON = response.result.value{
                let locationObject: Dictionary = locationJSON as! Dictionary<String, Any>
                let locations = locationObject["location"]! as! [String: Any]
                self.countryCode = locations["calling_code"]! as! String
            }
        }
        
        var contacts = [CNContact]()
        group.enter()
        DispatchQueue.global(qos: .background).async {
            let keys = [CNContactGivenNameKey, CNContactFamilyNameKey, CNContactPhoneNumbersKey]
            let request = CNContactFetchRequest(keysToFetch: keys as [CNKeyDescriptor])
            do {
                try CNContactStore().enumerateContacts(with: request) { (contact, stop) in
                    DispatchQueue.main.async {
                        contacts.append(contact)
                    }
                }
            } catch {
                print(error.localizedDescription)
            }
            group.leave()
            self.listContactFromPhone(contacts: contacts)
        }
        
        group.enter()
        MessageService.shared.fetchMessages(onSuccess: { (contacts) in
            group.leave()
            self.contacts = contacts
        }) { (error) in
            group.leave()
        }
        
        group.notify(queue: .main) {
            self.dismissHUD()
            self.tableView.reloadData()
        }
    }
    
    func listContactFromPhone(contacts: [CNContact]) {
        for i in 0..<contacts.count{
            for phoneNumber in contacts[i].phoneNumbers {
                guard let number = phoneNumber.value as? CNPhoneNumber else {
                    return
                }
                if number.stringValue.hasPrefix("0") {
                    self.tempNumber = number.stringValue.replacingCharacters(in: ...number.stringValue.startIndex, with: countryCode)
                }
                else if !number.stringValue.hasPrefix("+") {
                    self.tempNumber = countryCode + number.stringValue
                }
                else {
                    self.tempNumber = number.stringValue
                }
            }
            tempPhoneModel.append(ContactFromPhone(name: "\(contacts[i].givenName) \(contacts[i].familyName)", number: tempNumber!))
        }
    }
    
    func deleteConversation(indexPath: IndexPath) {
        let actionSheet = UIAlertController(title: "Deseas borrar esta conversacion?", message: nil, preferredStyle: .actionSheet)
        
        let yesAction = UIAlertAction(title: "Sí", style: .destructive) { [weak self] (alertAction) -> Void in
            guard let strongSelf = self else { return }
            
            let contact = strongSelf.contacts[indexPath.row]
            
            strongSelf.showHUD()
            MessageService.shared.deleteConversation(phone: contact.contactPhone, onSuccess: { (json, error) in
                Thread.runInMain {
                    strongSelf.dismissHUD()
                    strongSelf.contacts.remove(at: indexPath.row)
                    
                    CATransaction.begin()
                    CATransaction.setCompletionBlock({
                        strongSelf.tableView.reloadData()
                    })
                    
                    strongSelf.tableView.beginUpdates()
                    strongSelf.tableView.deleteRows(at: [indexPath], with: .top)
                    strongSelf.tableView.endUpdates()
                    CATransaction.commit()
                }
            }, onError: { (error) in
                Thread.runInMain {
                    strongSelf.dismissHUD()
                }
            })
        }
        
        let noAction = UIAlertAction(title: "No", style: .cancel, handler: nil)
        
        actionSheet.addAction(yesAction)
        actionSheet.addAction(noAction)
        
        present(actionSheet, animated: true, completion: nil)
    }
}

// MARK: - UITableViewDataSource
extension MessageViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: MessagesTableViewCell.identifier, for: indexPath) as! MessagesTableViewCell
        let contact = contacts[indexPath.row]
        cell.renderData(contact: contact, indexPath: indexPath, countryCode: countryCode, tempPhoneModel: tempPhoneModel)
        
        cell.deleteAction = { [weak self] in
            self?.deleteConversation(indexPath: indexPath)
        }
        
        return cell
    }
}

// MARK: - UITableViewDelegate
extension MessageViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let contact = contacts[indexPath.row]
        let messageDetailVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MessageDetailVC") as! MessageDetailViewController
        messageDetailVC.contact = contact
        
        navigationController?.pushViewController(messageDetailVC, animated: true)
    }
}




//
//  webviewViewController.swift
//  Tapadito
//
//  Created by An Phan  on 3/17/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit
import AVFoundation
import ARKit
import WebKit

class WebViewController: UIViewController, WKUIDelegate ,WKNavigationDelegate {
    
    // MARK: - Variable
    var getDataURL: String?
    var getDataCell: HomeType?
    
    private var webView: WKWebView!
    
    // MARK: - View life cycle
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.navigationDelegate = self
        view = webView
    }
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myURL = URL(string: getDataURL!)
        let myRequest = URLRequest(url: myURL!)
        webView.load(myRequest)
        let refresh = UIBarButtonItem(barButtonSystemItem: .refresh, target: webView, action: #selector(webView.reload))
        toolbarItems = [refresh]
        navigationController?.isToolbarHidden = false
    }
    
    // MARK: - Private Methods
    private func webView(_ webView: WKWebView, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        if let serverTrust = challenge.protectionSpace.serverTrust {
            completionHandler(.useCredential, URLCredential(trust: serverTrust))
        }
    }
}

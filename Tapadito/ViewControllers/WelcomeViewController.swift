
//
//  HomeViewController.swift
//  Tapadito
//
//  Created by An Phan on 3/13/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

class WelcomeViewController: UIViewController {
    
    // MARK: - Variables
    var listHome:[HomeType] = homeList
    var listUrl = [WebUrl.price, WebUrl.features, WebUrl.getPhone, WebUrl.fag]
    var listReadMore = ["Ver precios", "Como funciona Tapadito", "Leer más"]
    var footerText: FooterDesc = footerDesc
    var headerText: HeaderDesc = headerDesc
    
    // MARK: - IBOutlet
    @IBOutlet weak var headerWrappedView: UIView!
    @IBOutlet weak var headerTitleLabel: UILabel!
    @IBOutlet weak var headerDesc1Label: UILabel!
    @IBOutlet weak var headerDesc2label: UILabel!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var logoLabel: UILabel!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var getNumberButton: UIButton!
    @IBOutlet weak var footerWrappedView: UIView!
    @IBOutlet weak var footerDescFirstLabel: UILabel!
    
    // MARK: - View life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        dynamicTable()
        prepareStyle()
        
        applyValueToHeaderLabel()
        applyValueToFooterLabel()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if let headerView = tableView.tableHeaderView {
            let height = headerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var headerFrame = headerView.frame
            
            //Comparison necessary to avoid infinite loop
            if height != headerFrame.size.height {
                headerFrame.size.height = height
                headerView.frame = headerFrame
                tableView.tableHeaderView = headerView
            }
        }

        if let footerView = tableView.tableFooterView {
            let height = footerView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize).height
            var footerFrame = footerView.frame

            //Comparison necessary to avoid infinite loop
            if height != footerFrame.size.height {
                footerFrame.size.height = height
                footerView.frame = footerFrame
                tableView.tableFooterView = footerView
            }
        }
        
        
    }
    
    // MARK: - Methods
    func prepareStyle() {
        
        headerWrappedView.customRoundify(coner: 7)
        headerWrappedView.layer.dropShadow()
        
        logoLabel.customRoundify(coner: 7)
        signUpButton.customRoundify(coner: 7)
        getNumberButton.customRoundify(coner: 7)
        
        footerWrappedView.customRoundify(coner: 7)
        footerWrappedView.layer.dropShadow()
    }
    
    func dynamicTable() {
        tableView.estimatedRowHeight = 200
        tableView.rowHeight = UITableView.automaticDimension
        tableView.reloadData()
    }
    
    func applyValueToHeaderLabel(){
        
        headerTitleLabel.text = headerDesc.title
        
        //NSRange to change color and font-weight for headerDescFirst
        let headerDescFirst = headerDesc.desc[0]
        let array = ["enviar", "recibir"]
        let headerAttrFirst = attributeText(label: footerDescFirstLabel, text: headerDescFirst, font1: UIFont.openSansRegular(size: 17), color1: "2E86C1", range1: "¡sin tener que dar su número de teléfono verdadero!", font2: UIFont.openSansBold(size: 17), color2: "444444", range2s: array, font3: UIFont.openSansRegular(size: 17), color3: "", range3: "")
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.3
        headerAttrFirst.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, headerAttrFirst.length))
        
        headerDesc1Label.attributedText = headerAttrFirst
        
        //NSRange to change color and font-weight for headerDescSecond
        //¡Su número de teléfono verdadero se que Tapadito!
        let headerDescSecond = headerDesc.desc[1]
        let headerAttrSecond = attributeText(label: footerDescFirstLabel, text: headerDescSecond, font1: UIFont.openSansBold(size: 17), color1: "444444", range1: "Tapadito", font2: UIFont.openSansBold(size: 17), color2: "444444", range2s: [""], font3: UIFont.openSansRegular(size: 17), color3: "", range3: "")
        let paragraphStyle2 = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.3
        headerAttrFirst.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle2, range:NSMakeRange(0, headerAttrSecond.length))
        
        headerDesc2label.attributedText = headerAttrSecond
    }
    
    func applyValueToFooterLabel(){
    
        let text = footerDesc.desc[0]
        let array = ["Tapadito", "sin darles su número de teléfono verdadero"]
        let arr = attributeText(label: footerDescFirstLabel, text: text, font1: UIFont.openSansRegular(size: 17), color1: "", range1: "", font2: UIFont.openSansBold(size: 17), color2: "444444", range2s: array, font3: UIFont.openSansRegular(size: 17), color3: "", range3: "")
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = 1.3
        arr.addAttribute(NSAttributedString.Key.paragraphStyle, value:paragraphStyle, range:NSMakeRange(0, arr.length))
        
        footerDescFirstLabel.attributedText = arr
    }

    // MARK: - IBAction
    @IBAction func signUpAction(_ sender: UIButton) {
        AppState.presentSignUp()
    }
    
    @IBAction func moreInfoAction(_ sender: UIButton) {
        let dataCell = listUrl.last
        performSegue(withIdentifier: "showWebViewVCID", sender: dataCell)
        
    }
    
    @IBAction func getNumberAction(_ sender: UIButton) {
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "SignUpViewController") as! SignUpViewController
        self.present(vc, animated: true, completion: nil)

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showWebViewVCID" {
            let destVC = segue.destination as? WebViewController
            destVC?.getDataURL = sender as? String
        }
    }
}

// MARK: - UITableViewDataSource
extension WelcomeViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listHome.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "WelcomeTableViewCell", for: indexPath) as! WelcomeTableViewCell
        
        switch indexPath.row {
        case 0:
            let text = listHome[indexPath.row].desc
            let arrRange2 = ["enviar", "recibir", "completamente gratis" ]
            let attr = attributeText(label: cell.descLabel, text: text, font1: UIFont.openSansBold(size: 17), color1: "008000", range1: "super barato", font2: UIFont.openSansBold(size: 17), color2: "444444", range2s: arrRange2, font3: UIFont.openSansBold(size: 17), color3: "008000", range3: "" )
            cell.descLabel.attributedText = attr
            
        case 1:
            let text = listHome[indexPath.row].desc
            let arrRange2 = ["todos nuestros países."]
            let attr = attributeText(label: cell.descLabel, text: text, font1: UIFont.openSansRegular(size: 17), color1: "0000ff", range1: "aquí en los Estados Unidos y en", font2: UIFont.openSansBold(size: 17), color2: "0000ff", range2s: arrRange2, font3: UIFont.openSansBold(size: 17), color3: "444444", range3: "•" )
            cell.descLabel.attributedText = attr
            
        case 2:
            let text = listHome[indexPath.row].desc
            let arrRange2 = [""]
            let attr = attributeText(label: cell.descLabel, text: text, font1: UIFont.openSansBold(size: 17), color1: "732626", range1: "Que alivio!", font2: UIFont.openSansBold(size: 17), color2: "444444", range2s: arrRange2, font3: UIFont.openSansBold(size: 24), color3: "008000", range3: "" )
            cell.descLabel.attributedText = attr
            
        default:
            break
        }
        
        cell.topImages.image = UIImage(named: listHome[indexPath.row].titleImage)
        cell.subTitleLabel.text = listHome[indexPath.row].subTitle
        
        return cell
    }
}

//
//  NotificationBannerView.swift
//  Tapadito
//
//  Created by An Phan on 3/20/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

enum AnimatedTime: Double {
    case fast = 1.0, middle = 2.0, slow = 4.0
}

final class NotificationBannerView: UIView {
    @IBOutlet public var alertLabel: UILabel!
    
    static func appearOn(controller: UIViewController?, text: String, animateTime: AnimatedTime = .middle, isNavi: Bool = false) {
        guard let controller = controller, controller.view.subviews.filter({ $0 is NotificationBannerView }).count == 0 else {
            return
        }
        
        let alertView: NotificationBannerView = NotificationBannerView.fromNib()
        let heightStatusNav = UIApplication.shared.statusBarFrame.height + 44
        let statusBarHeight = isNavi ? heightStatusNav : UIApplication.shared.statusBarFrame.height
        var startFrame = CGRect(origin: CGPoint(x: 0.0, y: statusBarHeight),
                                size: CGSize(width: UIScreen.main.bounds.width, height: 0.0))
        alertView.frame = startFrame
        alertView.alertLabel.text = text
        controller.view.addSubview(alertView)
        
        UIView.animate(withDuration: 0.5) {
            startFrame.size.height = 44.0
            alertView.frame = startFrame
            alertView.alertLabel.alpha = 1.0
        }
        
        let dispatchTime: DispatchTime = .now() + animateTime.rawValue
        
        DispatchQueue.main.asyncAfter(deadline: dispatchTime) {
            alertView.removeFromSuperview()
        }
    }
}

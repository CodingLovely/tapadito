//
//  Utilities.swift
//  Tapadito
//
//  Created by An Phan on 3/27/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import Foundation

func delay(_ delay:Double, closure:@escaping ()->()) {
    DispatchQueue.main.asyncAfter(
        deadline: DispatchTime.now() + Double(Int64(delay * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: closure)
}

//
//  LoadingHUD.swift
//  Tapadito
//
//  Created by An Phan on 3/20/19.
//  Copyright © 2019 Tapadito. All rights reserved.
//

import UIKit

final class LoadingHUD: UIActivityIndicatorView {
    fileprivate static var indicatorView: UIActivityIndicatorView!
    
    static func showHUD(in view: UIView?, enableInteract: Bool = false) {
        if view != nil {
            Thread.runInMain {
                view!.isUserInteractionEnabled = enableInteract
                indicatorView = UIActivityIndicatorView(style: .whiteLarge)
                indicatorView.color = .gray
                indicatorView.center = view!.center
                indicatorView.startAnimating()
                view!.addSubview(indicatorView)
            }
        }
    }
    
    static func hideHUD(in view: UIView?) {
        hideAllHUD(in: view)
    }
    
    static func hideAllHUD(in view: UIView?) {
        if view != nil {
            Thread.runInMain {
                for subview in view!.subviews where subview is UIActivityIndicatorView {
                    subview.removeFromSuperview()
                }
                view!.isUserInteractionEnabled = true
            }
        }
    }
}
